import { takeEvery, put, call, take } from "redux-saga/effects";
import {
  LOAD_POSTS,
  DELETE_POST,
  EDIT_POST,
  CREATE_POST,
  LOAD_POSTS_PAGINATION,
  LOAD_POSTS_BY_USER,
  LOAD_POSTS_BY_USER_PAGINATION,
  GET_POST_DETAIL,
  GET_USER_RESPONSE,
} from "../actions/actionsType";
import axios from "axios";
import {
  postsToShowFunc,
  postsResponse,
  postsPaginationResponse,
  deletePostResponse,
  editPostResponse,
  createPostResponse,
  postsByUserResponse,
  postsByUserPaginationResponse,
  getPostDetailResponse,
} from "../actions/actionCreator";
/* LOAD POSTS */
async function loadPostsFromSaga() {
  console.log("LOad FROM SAGA");
  let posts = await axios
    .get("https://finalprojectapi.magisoft.solutions/posts/", {
      withCredentials: true,
    })
    .then((res) => res.data);
  return posts;
}

async function loadPostsPaginationFromSaga(page) {
  let skip = 6 * page - 6;
  let posts = await axios
    .get(
      "https://finalprojectapi.magisoft.solutions/posts/",
      {
        headers: {
          "Content-Type": "application/json",
        },
        params: {
          skip: skip,
          limit: 6,
        },
      },
      { withCredentials: true }
    )
    .then((res) => res.data);
  return posts;
}
/* LOAD POSTS BY USER */
async function loadPostsByUserFromSaga(id) {
  console.log("form saga id", id);
  let posts = await axios
    .get(`https://finalprojectapi.magisoft.solutions/users/${id}/posts`, {
      withCredentials: true,
    })
    .then((res) => res.data)
    .catch((e) => e);
  return posts;
}
async function loadPostsByUserPaginationFromSaga(payload) {
  let skip = 6 * payload.page - 6;
  let posts = await axios
    .get(
      `https://finalprojectapi.magisoft.solutions/users/${payload.id}/posts/`,
      {
        headers: {
          "Content-Type": "application/json",
        },
        params: {
          skip: skip,
          limit: 6,
        },
        withCredentials: true,
      }
    )
    .then((res) => res.data);
  return posts;
}

/* DELETE EDIT CREATE */
async function deletePostFromSaga(id) {
  let post = await axios
    .delete(`https://finalprojectapi.magisoft.solutions/posts/${id}`, {
      withCredentials: true,
    })
    .then((res) => res);
  return post;
}
async function editPostFromSaga(id, payload) {
  let post = await axios
    .patch(`https://finalprojectapi.magisoft.solutions/posts/${id}`, payload, {
      withCredentials: true,
    })
    .then((res) => res);
  return post;
}
async function createPostFromSaga(payload) {
  let post = await axios
    .post(`https://finalprojectapi.magisoft.solutions/posts/`, payload, {
      withCredentials: true,
    })
    .then((res) => res);
  return post;
}

async function getPostFromSaga(id) {
  let posts = await axios
    .get(`https://finalprojectapi.magisoft.solutions/posts/${id}`, {
      withCredentials: true,
    })
    .then((res) => res.data);
  return posts;
}

/* Workers */
/* ALLPOSTS */
export function* loadPostsSaga() {
  const data = yield call(loadPostsFromSaga);
  yield put(postsResponse(data.results));
  yield put(postsToShowFunc(data.results));
}

export function* loadPostsPaginationSaga(action) {
  const data = yield call(loadPostsPaginationFromSaga, action.payload);

  yield put(postsPaginationResponse(data));
}
/* BY USERS */
export function* loadPostsByUserSaga(action) {
  try {
    const data = yield call(loadPostsByUserFromSaga, action.payload);
    yield put(postsByUserResponse(data.results));
    yield put(postsToShowFunc(data.results));
  } catch (e) {
    console.log(e);
  }
}
export function* loadPostsByUserPaginationSaga(action) {
  const data = yield call(loadPostsByUserPaginationFromSaga, action.payload);

  yield put(postsByUserPaginationResponse(data));
}
/* DELTE EDIT CREATE POSTS */
export function* deletePostSaga(action) {
  const deletedPost = yield call(deletePostFromSaga, action.payload);

  yield put(deletePostResponse());
  yield call(loadPostsSaga);
}

export function* editPostSaga(action) {
  const editPost = yield call(editPostFromSaga, action.id, action.payload);

  yield put(editPostResponse());
  yield call(loadPostsSaga);
}
export function* createPostSaga(action) {
  const createdPost = yield call(createPostFromSaga, action.payload);

  yield put(createPostResponse());
  yield call(loadPostsSaga);
}
export function* getPostSaga(action) {
  const data = yield call(getPostFromSaga, action.id);
  yield put(getPostDetailResponse(data));
}

/* Watchers */

export function* watchPostsSaga() {
  yield takeEvery(LOAD_POSTS, loadPostsSaga);
  yield takeEvery(LOAD_POSTS_PAGINATION, loadPostsPaginationSaga);
  yield takeEvery(LOAD_POSTS_BY_USER, loadPostsByUserSaga);
  yield takeEvery(LOAD_POSTS_BY_USER_PAGINATION, loadPostsByUserPaginationSaga);
  yield takeEvery(DELETE_POST, deletePostSaga);
  yield takeEvery(EDIT_POST, editPostSaga);
  yield takeEvery(CREATE_POST, createPostSaga);
  yield takeEvery(GET_POST_DETAIL, getPostSaga);
}
