import { all } from "redux-saga/effects";
import { watchAuthSaga } from "./authSaga";
import { watchPostsSaga } from "./postsSaga";
import { watchUsersSaga } from "./usersSaga";
export function* rootSaga() {
  yield all([watchAuthSaga(), watchPostsSaga(), watchUsersSaga()]);
}
