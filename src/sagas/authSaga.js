import { takeEvery, put, call, putResolve, take } from "redux-saga/effects";
import {
  logIn,
  logInResponse,
  signInResponse,
  logOutResponse,
  checkAuthResponse,
  getUser,
  errorAuth,
} from "../actions/actionCreator";
import { LOG_IN, SIGN_IN, LOG_OUT, CHECK_AUTH } from "../actions/actionsType";
import axios from "axios";
import { store } from "../index";

async function logInInSaga(values) {
  return await axios
    .post("https://finalprojectapi.magisoft.solutions/auth/login", values, {
      withCredentials: true,
    })
    .then((res) => res);
  // .catch((e) => e.response);
}
async function signInInSaga(values) {
  let signIn = await axios
    .post("https://finalprojectapi.magisoft.solutions/auth/register", values)
    .then((res) => res);
  return signIn;
}
async function logOutInSaga() {
  let logOut = await axios
    .post("https://finalprojectapi.magisoft.solutions/auth/logout")
    .then((res) => res);
  return logOut;
}

async function checkAuthInSaga() {
  let users = await axios
    .get("https://finalprojectapi.magisoft.solutions/users", {
      withCredentials: true,
    })
    .then((res) => res)
    .catch((e) => e);
  return users;
}

/* worker */
export function* logInSaga(action) {
  try {
    const data = yield call(logInInSaga, action.payload);

    if (data.status > 199 && data.status < 301) {
      localStorage.setItem("id", data.data._id);
      yield put(logInResponse(data));
      yield put(getUser(data.data._id));
    }
  } catch (e) {
    console.log(e.response);
    yield put(logInResponse({}));
    yield put(errorAuth(e.message));
  }
}
export function* signInSaga(action) {
  try {
    const data = yield call(signInInSaga, action.payload);
    if (data.data.ok) {
      yield call(signInResponse);
      yield put(
        store.dispatch(
          logIn({ email: data.data.email, password: data.data.password })
        )
      );
    }
  } catch (e) {
    yield call(signInResponse);
    console.log(e.response);
  }
}
export function* logOutSaga() {
  const data = yield call(logOutInSaga);

  if (data.status === 200) {
    localStorage.removeItem("id");
  }
  yield put(logOutResponse());
}
export function* checkSaga() {
  let data = yield call(checkAuthInSaga);
  if (data) {
    yield put(checkAuthResponse(data));
  } else {
    yield put(checkAuthResponse(data));
  }
}
/* watcher */
export function* watchAuthSaga() {
  yield takeEvery(LOG_IN, logInSaga);
  yield takeEvery(SIGN_IN, signInSaga);
  yield takeEvery(LOG_OUT, logOutSaga);
  yield takeEvery(CHECK_AUTH, checkSaga);
}
