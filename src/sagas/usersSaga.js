import { takeEvery, put, call, take } from "redux-saga/effects";
import {
  DELETE_USER,
  EDIT_PHOTO,
  EDIT_USER_INFO,
  GET_USER,
  LOAD_USERS,
  LOAD_USERS_PAGINATION,
  GET_SPECIFIC_USER,
  CHECK_AUTH_RESPONSE,
  GET_USER_RESPONSE,
} from "../actions/actionsType";
import axios from "axios";
import {
  usersResponse,
  editPhotoResponse,
  editUserInfoResponse,
  getUserResponse,
  deleteUserResponse,
  usersPaginationResponse,
  getSpecificUserResponse,
  getUser,
} from "../actions/actionCreator";

async function loadUsersFromSaga() {
  let users = await axios
    .get("https://finalprojectapi.magisoft.solutions/users", {
      withCredentials: true,
    })
    .then((res) => res.data);
  return users;
}

async function editPhotoFromSaga(obj) {
  const form = new FormData();
  form.append("profileImg", obj.photo);
  let users = await axios
    .patch(
      `https://finalprojectapi.magisoft.solutions/users/uploadProfilePicture/${obj.id}`,
      form,
      { withCredentials: true }
    )
    .then((res) => console.log("response from saga", res));
  return users;
}
async function editUserInfoFromSaga(obj) {
  let users = await axios
    .patch(
      `https://finalprojectapi.magisoft.solutions/users/${obj.id}`,
      {
        name: obj.name,
        address: {
          city: obj.address.city,
          street: obj.address.street,
        },
        settings: {
          mode: obj.settings.mode,
        },
      },
      {
        withCredentials: true,
      }
    )
    .then((res) => res)
    .catch((e) => e);
  return users;
}

async function getUserFromSaga(id) {
  let user = await axios
    .get(`https://finalprojectapi.magisoft.solutions/users/${id}`, {
      withCredentials: true,
    })
    .then((res) => res)
    .catch((e) => e);
  return user;
}

async function getSpecificUserFromSaga(id) {
  console.log(id);

  let user = await axios
    .get(`https://finalprojectapi.magisoft.solutions/users/${id}`, {
      withCredentials: true,
    })
    .then((res) => res)
    .catch((e) => e);
  return user;
}

async function deleteUserFromSaga(id) {
  let user = await axios
    .delete(`https://finalprojectapi.magisoft.solutions/users/${id}`, {
      withCredentials: true,
    })
    .then((res) => console.log(res));
  return user;
}
async function loadUsersPaginationFromSaga(page) {
  let skip = 6 * page - 6;
  let users = await axios
    .get("https://finalprojectapi.magisoft.solutions/users", {
      headers: {
        "Content-Type": "application/json",
      },
      params: {
        skip: skip,
        limit: 6,
      },
      withCredentials: true,
    })
    .then((res) => res.data);
  return users;
}

/* WORKER */
export function* loadUsersSaga() {
  const data = yield call(loadUsersFromSaga);

  yield put(usersResponse(data));
}

export function* editPhotoSaga(action) {
  const data = yield call(editPhotoFromSaga, action.payload);

  yield put(editPhotoResponse());
}
export function* editUserInfoSaga(action) {
  try {
    const data = yield call(editUserInfoFromSaga, action.payload);
    console.log("data", data);
    yield put(getUser(data.data._id));
    yield put(editUserInfoResponse());
  } catch (e) {
    console.log("error in edit user saga", e);
  }
}
export function* getUserSaga(action) {
  try {
    const data = yield call(getUserFromSaga, action.payload);
    yield put(getUserResponse(data));
  } catch (e) {
    console.log(e);
  }
}
export function* getSpecificUserSaga(action) {
  try {
    const data = yield call(getSpecificUserFromSaga, action.payload);
    yield put(getSpecificUserResponse(data));
  } catch (e) {
    console.log(e);
  }
}
export function* deleteUserSaga(action) {
  const data = yield call(deleteUserFromSaga, action.payload);

  yield put(deleteUserResponse(data));
  yield call(loadUsersSaga);
}
export function* loadUserPaginationSaga(action) {
  const data = yield call(loadUsersPaginationFromSaga, action.payload);

  yield put(usersPaginationResponse(data));
}

/* Watchers */

export function* watchUsersSaga() {
  yield takeEvery(LOAD_USERS, loadUsersSaga);
  yield takeEvery(EDIT_PHOTO, editPhotoSaga);
  yield takeEvery(EDIT_USER_INFO, editUserInfoSaga);
  yield takeEvery(GET_USER, getUserSaga);
  yield takeEvery(GET_SPECIFIC_USER, getSpecificUserSaga);
  yield takeEvery(DELETE_USER, deleteUserSaga);
  yield takeEvery(LOAD_USERS_PAGINATION, loadUserPaginationSaga);
}
