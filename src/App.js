import { React, useEffect } from "react";
import { Route, Switch, Redirect } from "react-router";
import LoginPage from "./pages/LoginPage";
import RootPage from "./pages/RootPage";
import Header from "./components/header/Header";
import Footer from "./components/footer/Footer";
import { connect } from "react-redux";
import RegistrationPage from "./pages/RegistartionPage";
import PostsPage from "./pages/PostsPage";
import UsersPage from "./pages/UsersPage";
import MyProfile from "./pages/MyProfile";
import DeleteModal from "./components/modal/DeleteModal";
import CreateModal from "./components/modal/CreateModal";
import EditModal from "./components/modal/EditModal";
import { NotFound404 } from "./pages/NotFound404";
import UserPhotoModal from "./components/modal/UserPhotoModal";
import EditUserInfoModal from "./components/modal/EditUserInfoModal";
import { NotAutorized401 } from "./pages/NotAutorized401";
import MyPosts from "./pages/MyPosts";
import PostDetails from "./pages/PostDetails";
import DeleteUserModal from "./components/modal/DeleteUserModal";
import ProtectedRoute from "./components/protectedRoute/ProtectedRoute";
import Loader from "./components/loader/Loader";
import { checkAuth, getUser } from "../src/actions/actionCreator";
import UserDetail from "./pages/UserDetail";

import { makeStyles } from "@material-ui/styles";
import { Container, Grid, Box } from "@material-ui/core";
import Sidebar from "./components/sidebar/Sidebar";
import MainSection from "./components/mainSection/MainSection";
import axios from "axios";

const useStyles = makeStyles({
  section: {
    minHeight: "500px",
  },
  text: {
    textAlign: "center",
    paddingTop: "200px",
    fontSize: "40px",
    color: "#B8B8B8",
  },
});

function App(props) {
  const classes = useStyles();
  useEffect(() => {
    // axios
    //   .post("https://finalprojectapi.magisoft.solutions/auth/logout")
    //   .then((res) => res);
    props.checkAuth();
    if (localStorage.getItem("id")) {
      props.getUser(localStorage.getItem("id"));
    }
  }, []);

  if (props.isLoading) {
    return (
      <>
        <Header />
        <Loader />
        <Footer />
      </>
    );
  }
  return (
    <>
      <Header />
      <Container
        sx={{ maxWidth: "100vw !important", paddingLeft: "0 !important" }}
      >
        <Grid container className={classes.section}>
          <Grid item md={props.auth ? 2 : false}>
            <Sidebar />
          </Grid>
          <Grid item md={props.auth ? 10 : 12}>
            <MainSection>
              {props.isLoading ? (
                <Loader />
              ) : (
                <Switch>
                  <Route exact path="/" component={RootPage} />
                  {props.auth ? (
                    <Redirect from="/login" to="/" />
                  ) : (
                    <Route path="/login" component={LoginPage} />
                  )}
                  <Route path="/register" component={RegistrationPage} />
                  <ProtectedRoute exact path="/posts" component={PostsPage} />
                  <ProtectedRoute path="/posts/:id" component={PostDetails} />
                  <ProtectedRoute exact path="/users" component={UsersPage} />
                  <ProtectedRoute path="/users/:id" component={UserDetail} />
                  <ProtectedRoute exact path="/myposts" component={MyPosts} />
                  <ProtectedRoute
                    exact
                    path="/myprofile"
                    component={MyProfile}
                  />
                  <Route exact path="/notfound404" component={NotFound404} />
                  <Route path="/notautorized401" component={NotAutorized401} />
                  <Redirect from="/*" to="/notfound404" />
                </Switch>
              )}
            </MainSection>
          </Grid>
        </Grid>
      </Container>
      <Footer />
      <DeleteModal />
      <CreateModal />
      <EditModal />
      <UserPhotoModal />
      <EditUserInfoModal />
      <DeleteUserModal />
    </>
  );
}

function NoAuthRoutes() {
  return (
    <Switch>
      <Redirect from="/posts" to="/notautorized401" />
      <Redirect from="/users" to="/notautorized401" />
      <Redirect from="/myprofile" to="/notautorized401" />
      <Redirect from="/myposts" to="/notautorized401" />
      <Redirect from="/postdetails" to="/notautorized401" />
      <Redirect from="/" to="/notfound404" />
    </Switch>
  );
}

function AuthRoutes() {
  return (
    <Switch>
      <Route exact path="/" component={RootPage} />
      <Route path="/login" component={RootPage} />
      <Route path="/register" component={RootPage} />
      <Route path="/posts" component={PostsPage} />
      <Route path="/users" component={UsersPage} />
      <Route path="/myposts" component={MyPosts} />
      <Route path="/myprofile" component={MyProfile} />
      <Route path="/postdetails" component={PostDetails} />
    </Switch>
  );
}
const mapStateToProps = (state) => ({
  isLoading: state.auth.isLoading,
  isLoadingUsers: state.users.isLoading,
  auth: state.auth.isAuth,
});
const mapDispatchToProps = (dispatch) => ({
  checkAuth: () => dispatch(checkAuth()),
  getUser: (id) => dispatch(getUser(id)),
});
export default connect(mapStateToProps, mapDispatchToProps)(App);
