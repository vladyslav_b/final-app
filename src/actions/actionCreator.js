import {
  LOG_IN,
  LOG_IN_RESPONSE,
  LOG_OUT,
  LOG_OUT_RESPONSE,
  SIGN_IN,
  SIGN_IN_RESPONSE,
  CHECK_AUTH,
  CHECK_AUTH_RESPONSE,
  LOAD_POSTS,
  LOAD_POSTS_RESPONSE,
  LOAD_POSTS_PAGINATION,
  LOAD_POSTS_PAGINATION_RESPONSE,
  LOAD_POSTS_BY_USER,
  LOAD_POSTS_BY_USER_RESPONSE,
  LOAD_POSTS_BY_USER_PAGINATION,
  LOAD_POSTS_BY_USER_PAGINATION_RESPONSE,
  LOAD_POSTS_BY_SELECTED_USER,
  EDIT_POST,
  EDIT_POST_RESPONSE,
  DELETE_POST,
  DELETE_POST_RESPONSE,
  CHOOSE_POST,
  FULL_INFO_CHOOSE_POST,
  CREATE_POST,
  CREATE_POST_RESPONSE,
  SEARCH_POSTS,
  LOAD_USERS,
  LOAD_USERS_RESPONSE,
  EDIT_USER_INFO,
  EDIT_USER_INFO_RESPONSE,
  CREATE_MODAL_OPEN,
  CREATE_MODAL_CLOSE,
  EDIT_MODAL_OPEN,
  EDIT_MODAL_CLOSE,
  DELETE_MODAL_OPEN,
  DELETE_MODAL_CLOSE,
  EDIT_PHOTO_MODAL_OPEN,
  EDIT_PHOTO_MODAL_CLOSE,
  EDIT_USER_INFO_MODAL_OPEN,
  EDIT_USER_INFO_MODAL_CLOSE,
  DELETE_USER_MODAL_OPEN,
  DELETE_USER_MODAL_CLOSE,
  EDIT_PHOTO,
  EDIT_PHOTO_RESPONSE,
  GET_USER,
  GET_USER_RESPONSE,
  CHOOSE_USER,
  DELETE_USER,
  DELETE_USER_RESPONSE,
  LOAD_USERS_PAGINATION,
  LOAD_USERS_PAGINATION_RESPONSE,
  SEARCH_USERS,
  POSTS_TO_SHOW,
  GET_POST_DETAIL,
  GET_POST_DETAIL_RESPONSE,
  GET_SPECIFIC_USER,
  GET_SPECIFIC_USER_RESPONSE,
  ERROR_AUTH,
} from "./actionsType";
// AUTH
export function logIn(payload) {
  console.log("Login", payload);
  return {
    type: LOG_IN,
    payload,
  };
}

export function logInResponse(payload) {
  console.log("loin response", payload);
  if (payload.status < 300) {
    return {
      type: LOG_IN_RESPONSE,
      payload,
      isAuth: true,
    };
  } else {
    return {
      type: LOG_IN_RESPONSE,
      payload,
      isAuth: false,
    };
  }
}
export function logOut() {
  return {
    type: LOG_OUT,
  };
}
export function logOutResponse() {
  return {
    type: LOG_OUT_RESPONSE,
  };
}

export function signIn(payload) {
  return {
    type: SIGN_IN,
    payload,
  };
}
export function signInResponse() {
  return {
    type: SIGN_IN_RESPONSE,
  };
}

export function checkAuth() {
  return {
    type: CHECK_AUTH,
  };
}
export function checkAuthResponse(payload) {
  let auth;
  if (payload?.status > 199 && payload?.status < 300) {
    auth = true;
    return {
      type: CHECK_AUTH_RESPONSE,
      payload: auth,
    };
  } else {
    auth = false;
    return {
      type: CHECK_AUTH_RESPONSE,
      payload: auth,
    };
  }
}
// POSTS
export function loadPosts() {
  console.log("LAOD FROM ACT");
  return {
    type: LOAD_POSTS,
  };
}
export function postsResponse(payload) {
  return {
    type: LOAD_POSTS_RESPONSE,
    payload,
  };
}

export function loadPostsPagination(payload) {
  console.log("LAOD FROM ACT");
  return {
    type: LOAD_POSTS_PAGINATION,
    payload,
  };
}
export function postsPaginationResponse(payload) {
  return {
    type: LOAD_POSTS_PAGINATION_RESPONSE,
    payload,
  };
}
export function loadPostsByUser(payload) {
  return {
    type: LOAD_POSTS_BY_USER,
    payload,
  };
}
export function postsByUserResponse(payload) {
  return {
    type: LOAD_POSTS_BY_USER_RESPONSE,
    payload,
  };
}
export function loadPostsByUserPagination(payload) {
  return {
    type: LOAD_POSTS_BY_USER_PAGINATION,
    payload,
  };
}
export function postsByUserPaginationResponse(payload) {
  return {
    type: LOAD_POSTS_BY_USER_PAGINATION_RESPONSE,
    payload,
  };
}
export function loadPostsBySeletedUser(payload) {
  let newArr;
  if (payload.id == "all") {
    newArr = payload.posts;
  } else {
    newArr = payload.posts.filter((item) => item.author._id === payload.id);
  }

  return {
    type: LOAD_POSTS_BY_SELECTED_USER,
    payload: newArr,
  };
}

export function editPost(id, payload) {
  console.log("edit from action", id, payload);
  return {
    type: EDIT_POST,
    id,
    payload,
  };
}
export function editPostResponse() {
  return {
    type: EDIT_POST_RESPONSE,
  };
}
export function deletePost(payload) {
  console.log("DELETEDpost )", payload);
  return {
    type: DELETE_POST,
    payload,
  };
}
export function deletePostResponse() {
  return {
    type: DELETE_POST_RESPONSE,
  };
}

export function choosePost(payload) {
  return {
    type: CHOOSE_POST,
    payload,
  };
}
export function fullInfoChoosePost(payload) {
  return {
    type: FULL_INFO_CHOOSE_POST,
    payload,
  };
}
export function createPost(payload) {
  console.log("create", payload);
  return {
    type: CREATE_POST,
    payload,
  };
}
export function createPostResponse() {
  console.log("create Response");
  return {
    type: CREATE_POST_RESPONSE,
  };
}
export function searchPosts(payload) {
  let searchArr = payload.posts.filter(
    (item) => item.title.indexOf(payload.word) > -1
  );

  return {
    type: SEARCH_POSTS,
    payload: searchArr,
  };
}
export function postsToShowFunc(payload) {
  return {
    type: POSTS_TO_SHOW,
    payload,
  };
}
export function getPostDetail(id) {
  return {
    type: GET_POST_DETAIL,
    id,
  };
}
export function getPostDetailResponse(payload) {
  return {
    type: GET_POST_DETAIL_RESPONSE,
    payload,
  };
}
// USERS

export function getUser(payload) {
  console.log("get user");
  return {
    type: GET_USER,
    payload,
  };
}
export function getUserResponse(payload) {
  return {
    type: GET_USER_RESPONSE,
    payload,
  };
}
export function getSpecificUser(payload) {
  return {
    type: GET_SPECIFIC_USER,
    payload,
  };
}
export function getSpecificUserResponse(payload) {
  return {
    type: GET_SPECIFIC_USER_RESPONSE,
    payload,
  };
}
export function loadUsers() {
  return {
    type: LOAD_USERS,
  };
}
export function usersResponse(payload) {
  return {
    type: LOAD_USERS_RESPONSE,
    payload,
  };
}
export function loadUsersPagination(payload) {
  console.log("load users", payload);
  return {
    type: LOAD_USERS_PAGINATION,
    payload,
  };
}
export function usersPaginationResponse(payload) {
  return {
    type: LOAD_USERS_PAGINATION_RESPONSE,
    payload,
  };
}

export function editPhoto(payload) {
  return {
    type: EDIT_PHOTO,
    payload,
  };
}
export function editPhotoResponse(payload) {
  return {
    type: EDIT_PHOTO_RESPONSE,
    payload,
  };
}

export function editUserInfo(payload) {
  return {
    type: EDIT_USER_INFO,
    payload,
  };
}
export function editUserInfoResponse(payload) {
  return {
    type: EDIT_USER_INFO_RESPONSE,
    payload,
  };
}
export function chooseUser(payload) {
  return {
    type: CHOOSE_USER,
    payload,
  };
}

export function deleteUser(payload) {
  return {
    type: DELETE_USER,
    payload,
  };
}
export function deleteUserResponse(payload) {
  return {
    type: DELETE_USER_RESPONSE,
    payload,
  };
}

export function searchUsers(payload) {
  console.log(payload);
  let searchArr = payload.users.filter(
    (item) => item?.name.indexOf(payload.word) > -1
  );
  console.log("ss", searchArr);
  return {
    type: SEARCH_USERS,
    payload: searchArr,
  };
}
// Modal

export function createModalOpen() {
  return {
    type: CREATE_MODAL_OPEN,
  };
}
export function createModalClose() {
  return {
    type: CREATE_MODAL_CLOSE,
  };
}

export function editModalOpen() {
  return {
    type: EDIT_MODAL_OPEN,
  };
}
export function editModalClose() {
  return {
    type: EDIT_MODAL_CLOSE,
  };
}

export function deleteModalOpen() {
  return {
    type: DELETE_MODAL_OPEN,
  };
}
export function deleteModalClose() {
  return {
    type: DELETE_MODAL_CLOSE,
  };
}

export function editUserInfoModalOpen() {
  console.log("click");
  return {
    type: EDIT_USER_INFO_MODAL_OPEN,
  };
}
export function editUserInfoModalClose() {
  return {
    type: EDIT_USER_INFO_MODAL_CLOSE,
  };
}

export function editPhotoModalOpen() {
  return {
    type: EDIT_PHOTO_MODAL_OPEN,
  };
}
export function editPhotoModalClose() {
  return {
    type: EDIT_PHOTO_MODAL_CLOSE,
  };
}

export function deleteUserModalOpen() {
  return {
    type: DELETE_USER_MODAL_OPEN,
  };
}
export function deleteUserModalClose() {
  return {
    type: DELETE_USER_MODAL_CLOSE,
  };
}

//Errors

export function errorAuth(payload) {
  console.log(payload);
  return {
    type: ERROR_AUTH,
    payload,
  };
}
