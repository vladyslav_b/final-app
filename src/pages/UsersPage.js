import { React, useEffect } from "react";
import {
  Button,
  Container,
  Grid,
  Box,
  TextField,
  InputAdornment,
  IconButton,
} from "@material-ui/core";
import {
  loadUsers,
  loadUsersPagination,
  searchUsers,
} from "../actions/actionCreator";
import { connect } from "react-redux";
import TablePostOrUser from "../components/table/TablePostOrUser";
import CardsPostOrUser from "../components/card/CardsPostOrUser";
import Loader from "../components/loader/Loader";
import { Formik, Field, Form } from "formik";
import { Search } from "@material-ui/icons";

function UsersPage({
  isLoading,
  users,
  mode,
  loadUsers,
  loadUsersPagination,
  usersPagination,
  searchUsers,
  sUsers,
}) {
  useEffect(() => {
    loadUsers();
  }, []);
  if (isLoading) {
    return <Loader />;
  } else {
    return (
      <>
        <Box component="h2" sx={{ textAlign: "center" }}>
          Users Page
        </Box>
        <Box component="h6" sx={{ textAlign: "center", color: "red" }}>
          "After delete,edit,create post or user click on pagiantion to see
          changes(only for cards)"
          <br />
        </Box>
        {/* <Button
          onClick={() => {
            loadUsers();
          }}
        >
          LOAD ALL USERS
        </Button> */}
        {/* <Button
          onClick={() => {
            loadUsersPagination(1);
          }}
        >
          LOAD ALL USERS WITH PAGINATION(CARD ONLY)
        </Button> */}
        {/* <h3>Search User By Name(Only for Table)</h3>
        <Formik
          initialValues={{
            search: "",
          }}
          onSubmit={async (values) => {
            searchUsers({ word: values.search, users: users });
          }}
        >
          <Form>
            <label htmlFor="search">Search Users</label>
            <Field id="search" name="search" placeholder="Enter user name" />

            <button type="submit">Search mthfk</button>
          </Form>
        </Formik> */}

        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <Formik
            initialValues={{
              search: "",
            }}
            onSubmit={async (values) => {
              console.log(values);
              searchUsers({ word: values.search, users: users });
            }}
          >
            {(props) => (
              <Form>
                <TextField
                  sx={{ width: "310px" }}
                  id="search"
                  name="search"
                  variant="standard"
                  placeholder="Search"
                  onChange={props.handleChange}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="search"
                          onClick={props.handleSubmit}
                          edge="end"
                        >
                          <Search />
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Form>
            )}
          </Formik>
        </Box>

        {mode === "TABLE" ? (
          <TablePostOrUser
            target="users"
            postsOrUser={sUsers ? sUsers : users}
          />
        ) : (
          <CardsPostOrUser
            postsOrUser={usersPagination}
            disp="3"
            target="users"
          />
        )}
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  isLoading: state.auth.isLoading,
  mode: state.users.user?.data?.settings?.mode,
  users: state.users.loadedUsers,
  usersPagination: state.users.loadedUsersPagination,
  sUsers: state.users.searchUsers,
});

const mapDispatchToProps = (dispatch) => ({
  loadUsers: () => dispatch(loadUsers()),
  loadUsersPagination: (page) => dispatch(loadUsersPagination(page)),
  searchUsers: (payload) => dispatch(searchUsers(payload)),
});
export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);
