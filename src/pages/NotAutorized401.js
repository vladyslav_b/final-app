import { Typography } from "@material-ui/core";
import { Box } from "@material-ui/system";

export function NotAutorized401() {
  return (
    <Box
      sx={{
        display: "block",
        minHeight: "300px",
        textAlign: "center",
        padding: "50px",
        fontSize: "4em",
        color: "#B8B8B8",
      }}
    >
      <Typography
        sx={{ fontWeight: "bold", fontSize: "140px", lineHeight: "164px" }}
      >
        401
      </Typography>
      <Typography sx={{ fontSize: "18px" }}>AUTHORIZATION REQIRED</Typography>
    </Box>
  );
}
