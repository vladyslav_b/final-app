import { React, useEffect, useState } from "react";
import {
  Button,
  Box,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  TextField,
  InputAdornment,
  IconButton,
} from "@material-ui/core";
import {
  postsToShowFunc,
  loadPosts,
  loadPostsPagination,
  createModalOpen,
  searchPosts,
  loadUsers,
  loadPostsBySeletedUser,
  getUser,
  checkAuth,
} from "../actions/actionCreator";
import { connect } from "react-redux";
import TablePostOrUser from "../components/table/TablePostOrUser";
import CardsPostOrUser from "../components/card/CardsPostOrUser";
import { Formik, Field, Form } from "formik";
import Loader from "../components/loader/Loader";
import { Search } from "@material-ui/icons";

function PostsPage({
  auth,
  isLoading,
  checkAuth,
  getUser,
  postsToShowFunc,
  mode,
  posts,
  postsToShow,
  users,
  sPosts,
  loadPosts,
  loadUsers,
  postsPagination,
  loadPostsPagination,
  createModalOpen,
  searchPosts,
  loadPostsBySeletedUser,
}) {
  const [name, setName] = useState("");

  const handleChange = (event) => {
    setName(event.target.value);
    loadPostsBySeletedUser({ id: event.target.value, posts: posts });
  };

  useEffect(() => {
    loadUsers();
  }, []);
  useEffect(() => {
    postsToShowFunc(sPosts);
  }, [sPosts]);
  if (isLoading) {
    return <Loader />;
  } else {
    return (
      <>
        <Box component="h2" sx={{ textAlign: "center" }}>
          POST PAGE
        </Box>
        <Box component="h6" sx={{ textAlign: "center", color: "red" }}>
          "After delete,edit,create post or user click on pagiantion to see
          changes(only for cards)"
          <br />
        </Box>
        <Box
          sx={{
            display: "flex",
            maxHeight: "60px",
            justifyContent: "space-between",
          }}
        >
          <Box>
            <Button
              variant="text"
              sx={{ color: "#6C6C6C" }}
              onClick={() => {
                createModalOpen();
              }}
            >
              + Create new post
            </Button>
          </Box>
          {/* <Button
          onClick={() => {
            searchPosts({ word: "asdasdasddsa", posts: [] });
            loadPosts();
          }}
        >
          LOAD ALL POSTS
        </Button> */}
          {/* <Button
          onClick={() => {
            loadPostsPagination(1);
          }}
        >
          LOAD ALL POSTS WITH PAGINATION(CARD ONLY)
        </Button> */}
          <Box>
            <Formik
              initialValues={{
                search: "",
              }}
              onSubmit={async (values) => {
                console.log(values);
                searchPosts({ word: values.search, posts: posts });
              }}
            >
              {(props) => (
                <Form>
                  <TextField
                    sx={{ width: "310px" }}
                    id="search"
                    name="search"
                    variant="standard"
                    placeholder="Search"
                    onChange={props.handleChange}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="search"
                            onClick={props.handleSubmit}
                            edge="end"
                          >
                            <Search />
                          </IconButton>
                        </InputAdornment>
                      ),
                    }}
                  />
                </Form>
              )}
            </Formik>
          </Box>
          {/* <Button
            variant="outlined"
            onClick={() => {
              loadUsers();
            }}
          >
            LOAD ALL USERS (for serach post hehe)
          </Button> */}
          <Box sx={{ m: 1, minWidth: 80, marginTop: "-15px" }}>
            <FormControl variant="standard" sx={{ width: "200px" }}>
              <InputLabel id="demo-simple-select-label">
                Filter by name
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={name}
                label="Age"
                onChange={handleChange}
              >
                <MenuItem value="all">
                  <em>ALL</em>
                </MenuItem>
                {users?.map((user) => {
                  return (
                    <MenuItem key={user._id} value={user._id}>
                      {user.name} , {user.email}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>
          </Box>
        </Box>
        {mode === "TABLE" ? (
          <TablePostOrUser target="posts" postsOrUser={postsToShow} />
        ) : (
          <CardsPostOrUser
            postsOrUser={postsPagination}
            disp="1"
            target="posts"
          />
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  isLoading: state.auth.isLoading,
  auth: state.auth.isAuth,
  mode: state.users.user?.data?.settings?.mode,
  postsToShow: state.posts.postsToShow,
  postsPagination: state.posts.loadPostsPagination,
  posts: state.posts.loadPosts,
  sPosts: state.posts.searchPosts,
  users: state.users.loadedUsers,
});

const mapDispatchToProps = (dispatch) => ({
  checkAuth: () => dispatch(checkAuth()),
  getUser: (id) => dispatch(getUser(id)),
  postsToShowFunc: (payload) => dispatch(postsToShowFunc(payload)),
  loadUsers: () => dispatch(loadUsers()),
  loadPosts: () => dispatch(loadPosts()),
  loadPostsPagination: (page) => dispatch(loadPostsPagination(page)),
  createModalOpen: () => dispatch(createModalOpen()),
  searchPosts: (payload) => dispatch(searchPosts(payload)),
  loadPostsBySeletedUser: (data) => dispatch(loadPostsBySeletedUser(data)),
});
export default connect(mapStateToProps, mapDispatchToProps)(PostsPage);
