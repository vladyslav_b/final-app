import { React, useState } from "react";
import MainSection from "../components/mainSection/MainSection";
import { connect } from "react-redux";
import {
  TextField,
  Button,
  Container,
  InputAdornment,
  IconButton,
  OutlinedInput,
  Box,
} from "@material-ui/core";
import { VisibilityOff, Visibility } from "@material-ui/icons";
import { signIn } from "../actions/actionCreator";
import * as yup from "yup";
import { useFormik } from "formik";

const validationSchema = yup.object({
  email: yup
    .string("Enter your email")
    .email("Enter a valid email")
    .required("Email is required"),
  password: yup
    .string("Enter your password")
    .min(8, "Password should be of minimum 8 characters length")
    .required("Password is required"),
});

function RegistrationPage(props) {
  const [values, setValues] = useState({
    showPassword: false,
  });

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };
  const formik = useFormik({
    initialValues: {
      email: "",
      name: "",
      password: "",
      city: "",
      street: "",
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      const obj = {
        email: values.email,
        name: values.name,
        password: values.password,
        address: {
          city: values.city,
          street: values.street,
        },
      };
      props.signIn(obj);
    },
  });
  return (
    <MainSection>
      <Box component="h2" sx={{ textAlign: "center" }}>
        REGISTRATION
      </Box>
      <Container sx={{ marginTop: "50px", width: "300px" }}>
        <form onSubmit={formik.handleSubmit}>
          <TextField
            fullWidth
            required
            id="email"
            name="email"
            label="Email"
            variant="standard"
            sx={{ marginBottom: "10px" }}
            autoComplete="false"
            value={formik.values.email}
            onChange={formik.handleChange}
            error={formik.touched.email && Boolean(formik.errors.email)}
            helperText={formik.touched.email && formik.errors.email}
          />
          <TextField
            fullWidth
            required
            id="name"
            name="name"
            label="Name"
            type="name"
            variant="standard"
            sx={{ marginBottom: "10px" }}
            value={formik.values.name}
            onChange={formik.handleChange}
            error={formik.touched.name && Boolean(formik.errors.name)}
            helperText={formik.touched.name && formik.errors.name}
          />
          <TextField
            fullWidth
            required
            id="password"
            name="password"
            label="Password"
            variant="standard"
            type={values.showPassword ? "text" : "password"}
            autoComplete="false"
            sx={{ marginBottom: "10px" }}
            value={formik.values.password}
            onChange={formik.handleChange}
            error={formik.touched.password && Boolean(formik.errors.password)}
            helperText={formik.touched.password && formik.errors.password}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
          <TextField
            fullWidth
            id="city"
            name="city"
            label="City"
            type="text"
            variant="standard"
            sx={{ marginBottom: "10px" }}
            value={formik.values.city}
            onChange={formik.handleChange}
            error={formik.touched.city && Boolean(formik.errors.city)}
            helperText={formik.touched.city && formik.errors.city}
          />
          <TextField
            fullWidth
            id="street"
            name="street"
            label="Street"
            type="text"
            variant="standard"
            sx={{ marginBottom: "10px" }}
            value={formik.values.street}
            onChange={formik.handleChange}
            error={formik.touched.street && Boolean(formik.errors.street)}
            helperText={formik.touched.street && formik.errors.street}
          />
          <Button
            color="primary"
            variant="contained"
            fullWidth
            type="submit"
            sx={{ marginTop: "40px", borderRadius: "0", height: "40px" }}
          >
            REGISTRATION
          </Button>
        </form>
      </Container>
    </MainSection>
  );
}
const mapDispatchToProps = (dispatch) => ({
  signIn: (payload) => dispatch(signIn(payload)),
});
export default connect(null, mapDispatchToProps)(RegistrationPage);
