import { React, useState } from "react";
import MainSection from "../components/mainSection/MainSection";
import { connect } from "react-redux";
import { useFormik, Formik } from "formik";
import {
  TextField,
  Button,
  Container,
  Box,
  Typography,
} from "@material-ui/core";
import { logIn } from "../actions/actionCreator";
import Loader from "../components/loader/Loader";

function LoginPage(props) {
  const [formErrors, setFormErrors] = useState({});
  const validate = (values) => {
    let errors = {};
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    if (!values.email) {
      errors.email = "Cannot be blank";
    } else if (!regex.test(values.email)) {
      errors.email = "Invalid email format";
    }
    if (!values.password) {
      errors.password = "Cannot be blank";
    } else if (values.password.length < 4) {
      errors.password = "Password must be more than 4 characters";
    }
    return errors;
  };
  console.log(formErrors);
  if (props.isLoading) {
    return (
      <MainSection>
        <Box component="h2" sx={{ textAlign: "center" }}>
          Log In
        </Box>
        <Container sx={{ margin: "50px auto", width: "500px" }}>
          <Loader />
        </Container>
      </MainSection>
    );
  } else {
    return (
      <MainSection>
        <Box component="h2" sx={{ textAlign: "center" }}>
          Log In
        </Box>
        <Container sx={{ margin: "50px auto", width: "300px" }}>
          <Formik
            initialValues={{
              email: "",
              password: "",
            }}
            onSubmit={(values, actions) => {
              setFormErrors(validate(values));
              props.logIn(values);
            }}
          >
            {(props) => (
              <form onSubmit={props.handleSubmit}>
                <TextField
                  fullWidth
                  id="email"
                  name="email"
                  label="Email"
                  sx={{ marginBottom: "10px" }}
                  value={props.values.email}
                  variant="standard"
                  onChange={props.handleChange}
                  error={props.touched.email && Boolean(props.errors.email)}
                  helperText={props.touched.email && props.errors.email}
                />
                <TextField
                  fullWidth
                  id="password"
                  name="password"
                  label="Password"
                  type="password"
                  variant="standard"
                  sx={{ marginBottom: "10px" }}
                  value={props.values.password}
                  onChange={props.handleChange}
                  error={
                    props.touched.password && Boolean(props.errors.password)
                  }
                  helperText={props.touched.password && props.errors.password}
                />

                <Button
                  color="primary"
                  variant="authBtn"
                  fullWidth
                  type="submit"
                  sx={{ marginTop: "40px", borderRadius: "0", height: "40px" }}
                >
                  <Typography variant="button">Login</Typography>
                </Button>
              </form>
            )}
          </Formik>
        </Container>
      </MainSection>
    );
  }
}
const mapStateToProps = (state) => ({
  isLoading: state.auth.isLoading,
});
const mapDispatchToProps = (dispatch) => ({
  logIn: (payload) => dispatch(logIn(payload)),
});
export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
