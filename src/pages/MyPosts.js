import { React, useEffect } from "react";
import MainSection from "../components/mainSection/MainSection";
import Sidebar from "../components/sidebar/Sidebar";
import {
  Button,
  Container,
  Grid,
  Box,
  TextField,
  InputAdornment,
  IconButton,
} from "@material-ui/core";
import {
  loadPostsByUserPagination,
  loadPostsByUser,
  createModalOpen,
  searchPosts,
  loadPosts,
  loadUsers,
  postsToShowFunc,
} from "../actions/actionCreator";
import { connect } from "react-redux";
import TablePostOrUser from "../components/table/TablePostOrUser";
import CardsPostOrUser from "../components/card/CardsPostOrUser";
import Loader from "../components/loader/Loader";
import { Search } from "@material-ui/icons";

import { Formik, Field, Form } from "formik";

function MyPosts({
  id,
  mode,
  sPosts,
  postsByUser,
  PostsByUserPagination,
  loadPostsByUser,
  loadPostsByUserPagination,
  createModalOpen,
  searchPosts,
  isLoading,
  isLoadingPost,
  loadPosts,
  loadUsers,
  postsToShowFunc,
}) {
  useEffect(() => {
    if (id) {
      loadPostsByUser(id);
    }
  }, [id]);
  useEffect(() => {
    postsToShowFunc(sPosts);
  }, [sPosts]);
  if (isLoading) {
    return <Loader />;
  } else if (id) {
    return (
      <>
        <Box component="h2" sx={{ textAlign: "center" }}>
          MY POST PAGE
        </Box>
        <Box component="h6" sx={{ textAlign: "center", color: "red" }}>
          "After delete,edit,create post or user click on pagiantion to see
          changes(only for cards)"
          <br />
        </Box>
        {/* <Button
          variant="outlined"
          onClick={() => {
            createModalOpen();
          }}
        >
          CREATE NEW POST
        </Button> */}
        {/* <Button
          onClick={() => {
            searchPosts({ word: "asdasdasddsa", posts: [] });
            loadPostsByUser(id);
          }}
        >
          LOAD ALL POSTS By USER
        </Button> */}
        {/* <Button
          onClick={() => {
            loadPostsByUserPagination({ page: 1, id: id });
          }}
        >
          LOAD ALL POSTS BY USER WITH PAGINATION(CARD ONLY)
        </Button> */}
        {/* <h3>Search Post (Only for Table)</h3>
        <Formik
          initialValues={{
            search: "",
          }}
          onSubmit={async (values) => {
            searchPosts({ word: values.search, posts: postsByUser });
          }}
        >
          <Form>
            <label htmlFor="search">Search Post</label>
            <Field id="search" name="search" placeholder="Enter post title" />

            <button type="submit">Search mthfk</button>
          </Form>
        </Formik> */}
        <Box sx={{ display: "flex", justifyContent: "center" }}>
          <Formik
            initialValues={{
              search: "",
            }}
            onSubmit={async (values) => {
              console.log(values);
              searchPosts({ word: values.search, posts: postsByUser });
            }}
          >
            {(props) => (
              <Form>
                <TextField
                  sx={{ width: "310px" }}
                  id="search"
                  name="search"
                  variant="standard"
                  placeholder="Search"
                  onChange={props.handleChange}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="search"
                          onClick={props.handleSubmit}
                          edge="end"
                        >
                          <Search />
                        </IconButton>
                      </InputAdornment>
                    ),
                  }}
                />
              </Form>
            )}
          </Formik>
        </Box>

        {mode === "TABLE" ? (
          <TablePostOrUser
            target="posts"
            postsOrUser={sPosts.length > 0 ? sPosts : postsByUser}
          />
        ) : (
          <CardsPostOrUser
            postsOrUser={PostsByUserPagination}
            idUser={id}
            disp="2"
            target="posts"
          />
        )}
      </>
    );
  } else {
    return null;
  }
}
const mapStateToProps = (state) => ({
  id: state.users.user?.data?._id,
  isLoading: state.auth.isLoading,
  isLoadingPost: state.posts.isLoading,
  mode: state.users.user?.data?.settings?.mode,
  PostsByUserPagination: state.posts.loadPostsByUserPagination,
  postsByUser: state.posts.postsByUser,
  sPosts: state.posts.searchPosts,
});

const mapDispatchToProps = (dispatch) => ({
  postsToShowFunc: (payload) => dispatch(postsToShowFunc(payload)),
  loadUsers: () => dispatch(loadUsers()),
  loadPosts: () => dispatch(loadPosts()),
  loadPostsByUser: (id) => dispatch(loadPostsByUser(id)),
  loadPostsByUserPagination: (page) =>
    dispatch(loadPostsByUserPagination(page)),
  createModalOpen: () => dispatch(createModalOpen()),
  searchPosts: (payload) => dispatch(searchPosts(payload)),
});
export default connect(mapStateToProps, mapDispatchToProps)(MyPosts);
