import { Box } from "@material-ui/system";
import { Typography } from "@material-ui/core";

export function NotFound404() {
  return (
    <Box
      sx={{
        display: "block",
        minHeight: "300px",
        textAlign: "center",
        padding: "50px",
        fontSize: "4em",
        color: "#B8B8B8",
      }}
    >
      <Typography
        sx={{ fontWeight: "bold", fontSize: "140px", lineHeight: "164px" }}
      >
        404
      </Typography>
      <Typography sx={{ fontSize: "18px" }}>NOT FOUND</Typography>
    </Box>
  );
}
