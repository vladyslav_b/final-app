import { React, useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Grid,
  Container,
  Typography,
  Box,
  Button,
  CardMedia,
  TextField,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import Loader from "../components/loader/Loader";
import {
  editPhotoModalOpen,
  editUserInfoModalOpen,
  getUser,
  loadUsers,
  chooseUser,
  getSpecificUser,
} from "../actions/actionCreator";
import { useParams } from "react-router";
import photoIcon from "../photo/photoIcon.png";

const useStyle = makeStyles({
  photoWrap: {
    width: "200px",
    height: "200px",
    margin: "0 auto",
  },
  photo: {
    width: "100%",
    height: "100%",
    borderRadius: "100%",
    margin: "33px 0 0 50px",
  },
});

function UserDetail(props) {
  const styless = useStyle();
  let { id } = useParams();

  useEffect(() => {
    props.getSpecificUser(id);
    console.log("data", props.data);
  }, []);

  if (props.isLoading || props.isLoadingUsers) {
    return (
      <>
        <Loader />
      </>
    );
  } else if (props.data?.name) {
    return (
      <>
        <Box sx={{ color: "#464646", fontSize: "20px", marginTop: "30px" }}>
          Progile details
        </Box>
        <Grid container>
          <Grid
            item
            md={4}
            sx={{ justifyContent: "center", alignItems: "center" }}
          >
            <Box className={styless.photoWrap}>
              <img
                className={styless.photo}
                src={
                  props.data?.profileImg
                    ? props.data?.profileImg
                    : "https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg"
                }
              />
            </Box>
          </Grid>
          <Grid item sx={{ maxWidth: "500px" }}>
            <Box sx={{ minHeight: "200px" }}>
              <TextField
                fullWidth
                id="name"
                name="name"
                label="Name"
                type="name"
                variant="standard"
                sx={{ marginBottom: "10px", marginTop: "10px" }}
                defaultValue={props.data.name}
                disabled
              />
              <TextField
                fullWidth
                id="email"
                name="email"
                label="Email"
                type="text"
                variant="standard"
                sx={{ marginBottom: "10px" }}
                value={props.data.email}
                disabled
              />
              <TextField
                fullWidth
                id="city"
                name="city"
                label="City"
                type="text"
                variant="standard"
                sx={{ marginBottom: "10px" }}
                value={props.data?.address?.city}
                disabled
              />
              <TextField
                fullWidth
                id="street"
                name="street"
                label="Street"
                type="text"
                variant="standard"
                sx={{ marginBottom: "10px" }}
                value={props.data?.address?.street}
                disabled
              />
            </Box>
          </Grid>
        </Grid>
      </>
    );
  } else {
    return null;
  }
}
const mapStateToProps = (state) => ({
  isLoading: state.auth.isLoading,
  isLoadingUsers: state.users.isLoading,
  loadedUsers: state.users.loadedUsers,
  data: state.users.specificUser.data,
});

const mapDispatchToProps = (dispatch) => ({
  getSpecificUser: (id) => dispatch(getSpecificUser(id)),
  loadUsers: () => dispatch(loadUsers()),
  chooseUser: (id) => dispatch(chooseUser(id)),
  editPhotoModalOpen: () => dispatch(editPhotoModalOpen()),
  editUserInfoModalOpen: () => dispatch(editUserInfoModalOpen()),
});
export default connect(mapStateToProps, mapDispatchToProps)(UserDetail);
