import React from "react";
import { connect } from "react-redux";
import { Container, Grid, Box } from "@material-ui/core";
import Sidebar from "../components/sidebar/Sidebar";
import MainSection from "../components/mainSection/MainSection";
import Loader from "../components/loader/Loader";
import { checkAuth, getUser } from "../actions/actionCreator";

import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  section: {
    minHeight: "500px",
  },
  text: {
    textAlign: "center",
    paddingTop: "200px",
    fontSize: "40px",
    color: "#B8B8B8",
  },
});

function RootPage({ auth, checkAuth, getUser }) {
  const classes = useStyles();
  if (auth) {
    return <Box className={classes.text}>Root Page</Box>;
  }
  return (
    <Box component="h2" className={classes.text}>
      Root Page
    </Box>
  );
}
const mapStateToProps = (state) => ({
  isLoading: state.auth.isLoading,
  auth: state.auth.isAuth,
});
const mapDispatchToProps = (dispatch) => ({
  checkAuth: () => dispatch(checkAuth()),
  getUser: (id) => dispatch(getUser(id)),
});
export default connect(mapStateToProps, mapDispatchToProps)(RootPage);
