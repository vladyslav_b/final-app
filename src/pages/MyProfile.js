import React from "react";
import { connect } from "react-redux";
import {
  Grid,
  Container,
  Typography,
  Box,
  Button,
  TextField,
  Avatar,
  CardMedia,
} from "@material-ui/core";
import { useFormik, Field } from "formik";
import { makeStyles } from "@material-ui/styles";
import Loader from "../components/loader/Loader";
import { editUserInfo, getUser, editPhoto } from "../actions/actionCreator";
import photoIcon from "../photo/photoIcon.png";

const useStyle = makeStyles({
  photoWrap: {
    position: "relative",
    width: "200px",
    height: "200px",
    marginLeft: "130px",
    marginRight: "30px",
    marginTop: "30px",
  },
  file: {
    display: "none !important",
  },
  photo: {
    width: "100%",
    height: "100%",
    borderRadius: "100%",
    zIndex: "1",
  },
  photoIcon: {
    position: "absolute",
    top: "0",
    borderRadius: "100%",
    zIndex: "99",
    opacity: "0",
    "&:hover": {
      opacity: "0.5",
      background: "#fff",
      cursor: "pointer",
    },
  },
});

function MyProfile(props) {
  const styless = useStyle();
  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: props.data.name || "",
      city: props.data?.address?.city || "",
      street: props.data?.address?.street || "",
      picked: props.data?.settings?.mode || "",
      file: props.data?.profileImg || "",
    },
    onSubmit: (values, { resetForm }) => {
      const payload = {
        id: props.data._id,
        photo: values.file,
      };
      const obj = {
        id: props.data._id,
        name: values.name,
        address: {
          city: values.city,
          street: values.street,
        },
        settings: {
          mode: values.picked,
        },
      };
      if (values.file != props.data?.profileImg) {
        props.editPhoto(payload);
      }

      props.editUserInfo(obj);
      // setTimeout(() => {
      //   props.getUser(props.data._id);
      // }, 2000);
    },
  });
  const [picture, setPicture] = React.useState(props.data.profileImg);
  const onChangePicture = (v) => {
    setPicture(URL.createObjectURL(v));
  };
  if (props.isLoading || props.isLoadingUser) {
    return <Loader />;
  } else {
    return (
      <>
        <Box sx={{ color: "#464646", fontSize: "20px", marginTop: "30px" }}>
          My profile
        </Box>
        <form onSubmit={formik.handleSubmit}>
          <Grid container>
            <Grid
              item
              md={4}
              sx={{ justifyContent: "center", alignItems: "center" }}
            >
              <Box className={styless.photoWrap}>
                <input
                  className={styless.file}
                  id="file"
                  name="file"
                  type="file"
                  onChange={(event) => {
                    onChangePicture(event.currentTarget.files[0]);
                    formik.setFieldValue("file", event.currentTarget.files[0]);
                  }}
                />
                <CardMedia
                  component="img"
                  src={photoIcon}
                  className={styless.photoIcon}
                  sx={{ width: "200px", height: "200px" }}
                  onClick={() => {
                    document.getElementById("file").click();
                  }}
                />
                <img
                  className={styless.photo}
                  src={
                    picture
                      ? picture
                      : "https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg"
                  }
                />
              </Box>
            </Grid>
            <Grid item sx={{ maxWidth: "500px" }}>
              <Box sx={{ minHeight: "200px" }}>
                <TextField
                  fullWidth
                  required
                  id="name"
                  name="name"
                  label="Name"
                  type="name"
                  variant="standard"
                  sx={{ marginBottom: "10px", marginTop: "10px" }}
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  error={formik.touched.name && Boolean(formik.errors.name)}
                  helperText={formik.touched.name && formik.errors.name}
                />
                <TextField
                  fullWidth
                  id="email"
                  name="email"
                  label="Email"
                  type="text"
                  variant="standard"
                  sx={{ marginBottom: "10px" }}
                  value={props.data.email}
                  disabled
                />
                <TextField
                  fullWidth
                  id="city"
                  name="city"
                  label="City"
                  type="text"
                  variant="standard"
                  sx={{ marginBottom: "10px" }}
                  value={formik.values.city}
                  onChange={formik.handleChange}
                  error={formik.touched.city && Boolean(formik.errors.city)}
                  helperText={formik.touched.city && formik.errors.city}
                />
                <TextField
                  fullWidth
                  id="street"
                  name="street"
                  label="Street"
                  type="text"
                  variant="standard"
                  sx={{ marginBottom: "10px" }}
                  value={formik.values.street}
                  onChange={formik.handleChange}
                  error={formik.touched.street && Boolean(formik.errors.street)}
                  helperText={formik.touched.street && formik.errors.street}
                />
                <label>
                  <input
                    type="radio"
                    name="picked"
                    value="TABLE"
                    checked={formik.values.picked === "TABLE"}
                    onChange={() => formik.setFieldValue("picked", "TABLE")}
                  />
                  TABLE
                </label>
                <label>
                  <input
                    type="radio"
                    name="picked"
                    value="BOXES"
                    checked={formik.values.picked === "BOXES"}
                    onChange={() => formik.setFieldValue("picked", "BOXES")}
                  />
                  BOXES
                </label>
              </Box>

              <Button
                type="submit"
                variant="contained"
                sx={{
                  display: "block",
                  margin: "40px 50px",
                  width: "170px",
                  height: "40px",
                  borderRadius: "0",
                }}
              >
                Save
              </Button>
            </Grid>
          </Grid>
        </form>
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  isLoading: state.auth.isLoading,
  isLoadingUser: state.users.isLoading,
  data: state.users.user.data ?? {},
});

const mapDispatchToProps = (dispatch) => ({
  editPhoto: (payload) => dispatch(editPhoto(payload)),
  editUserInfo: (values) => dispatch(editUserInfo(values)),
  getUser: (values) => dispatch(getUser(values)),
});
export default connect(mapStateToProps, mapDispatchToProps)(MyProfile);
