import { React, useEffect } from "react";
import MainSection from "../components/mainSection/MainSection";
import Sidebar from "../components/sidebar/Sidebar";
import Loader from "../components/loader/Loader";
import {
  Button,
  Container,
  Grid,
  Box,
  Typography,
  Avatar,
} from "@material-ui/core";
import { useParams } from "react-router";
import {
  loadPosts,
  loadPostsPagination,
  createModalOpen,
  getPostDetail,
} from "../actions/actionCreator";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles({
  wrapper: {
    width: "670px",
    marginLeft: "130px",
    marginTop: "40px",
  },
  title: {
    fontSize: "20px",
    color: "#464646 !important",
  },
  author: {
    margin: "10px 0",
    display: "flex",
    alignItems: "center",
  },
  avatar: {
    marginRight: "10px",
  },
  userName: {
    color: "#C4C4C4",
  },
  postImg: {
    width: "670px",
    height: "260px",
    backgroundColor: "#DDDDDD",
  },
  content: {
    width: "100%",
    padding: "10px",
    minHeight: "200px",
    color: "#6C6C6C",
  },
});

function PostDetails({
  post,
  isLoading,
  getPostDetail,
  loadPosts,
  loadedPosts,
}) {
  let postDetail;
  const classes = useStyles();
  let { id } = useParams();
  useEffect(() => {
    loadPosts();
    getPostDetail(id);
  }, []);

  postDetail = loadedPosts?.find((item) => item._id === post._id);

  console.log(postDetail);

  if (isLoading) {
    return <Loader />;
  } else {
    return (
      <Box className={classes.wrapper}>
        <Typography className={classes.title} variant="h5">
          {postDetail?.title}
        </Typography>
        <Box className={classes.author}>
          <Avatar
            className={classes.avatar}
            src={
              postDetail?.author?.profileImg
                ? postDetail?.author?.profileImg
                : "./public/user.png"
            }
          />

          <Box className={classes.userName}>{postDetail?.author?.name}</Box>
        </Box>
        <Box className={classes.postImg}></Box>
        <Typography className={classes.content}>
          {postDetail?.content}
        </Typography>
      </Box>
    );
  }
}
const mapStateToProps = (state) => ({
  post: state.posts.post,
  loadedPosts: state.posts.loadPosts,
  isLoading: state.auth.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  loadPosts: () => dispatch(loadPosts()),
  loadPostsPagination: (page) => dispatch(loadPostsPagination(page)),
  createModalOpen: () => dispatch(createModalOpen()),
  getPostDetail: (id) => dispatch(getPostDetail(id)),
});
export default connect(mapStateToProps, mapDispatchToProps)(PostDetails);
