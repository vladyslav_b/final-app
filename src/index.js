import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import App from "./App";
import { Provider } from "react-redux";
import { createStore, compose, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import { rootReducer } from "./reducers/rootReducer";
import { rootSaga } from "./sagas/rootSaga";
import { BrowserRouter as Router, HashRouter } from "react-router-dom";
import axios from "axios";
import { createTheme, ThemeProvider } from "@mui/material/styles";

axios.defaults.withCredentials = true;

const theme = createTheme({
  palette: {
    lite: {
      main: "#DDDDDD",
    },
    primary: {
      main: "#6C6C6C",
    },
    secondary: {
      main: "##C4C4C4",
    },
    darkest: {
      main: "##464646",
    },
  },
  typography: {
    fontFamily: ["Roboto"].join(","),
    button: {
      color: "#464646",
      fontSize: "16px",
    },
    cardTitle: {
      color: "#464646",
      fontSize: "20px",
    },
    cardContent: {
      color: "#6C6C6C",
      fontSize: "16px",
    },
  },
  components: {
    MuiButton: {
      variants: [
        {
          props: { variant: "chooseBtn" },
          style: {
            width: "170px",
            height: "40px",
            background: "#BABABA",
            borderRadius: "0",
            "&:hover": {
              background: "#545454;",
            },
          },
        },
        {
          props: { variant: "authBtn" },
          style: {
            width: "251px",
            height: "40px",
            borderRadius: "0",
            background: "#B8B8B8;",
            "&:hover": {
              background: "#545454;",
            },
          },
        },
      ],
    },
  },
});
const sagaMiddleware = createSagaMiddleware();
export const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(sagaMiddleware),
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
);

sagaMiddleware.run(rootSaga);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router history={HashRouter}>
        <ThemeProvider theme={theme}>
          <App />
        </ThemeProvider>
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
