import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Container, ListItem, List } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { getUser } from "../../actions/actionCreator";
const useStyles = makeStyles({
  wrap: {
    background: "#F3F3F3",
    height: "100%",
    width: "100%",
    padding: "0 !important",
  },
  item: {
    padding: "0 !important",
  },
  link: {
    color: "#6C6C6C",
    textDecoration: "none",
    width: "100%",
    height: "100%",

    padding: "13px 30px !important",
    fontSize: "16px",
    "&:hover": {
      color: "#6C6C6C",
      backgroundColor: "white",
    },
  },
});

function Sidebar(props) {
  const classes = useStyles();
  if (props.auth && localStorage.getItem("id")) {
    return (
      <Container
        className={classes.wrap}
        onClick={(event) => {
          if (event.target.tagName === "A") {
            sessionStorage.clear();
          }
        }}
      >
        <List>
          <ListItem className={classes.item}>
            <Link className={classes.link} to="/">
              Main
            </Link>
          </ListItem>
          <ListItem className={classes.item}>
            <Link className={classes.link} to="/posts">
              Posts
            </Link>
          </ListItem>
          <ListItem className={classes.item}>
            <Link className={classes.link} to="/users">
              Users
            </Link>
          </ListItem>
          <ListItem className={classes.item}>
            <Link className={classes.link} to="/myposts">
              My Posts
            </Link>
          </ListItem>
          <ListItem className={classes.item}>
            <Link className={classes.link} to="/myprofile">
              My Profile
            </Link>
          </ListItem>
        </List>
      </Container>
    );
  }

  return null;
  // <Container>
  //   <List>
  //     <ListItem>
  //       <Link className={classes.link} to="/">
  //         Main
  //       </Link>
  //     </ListItem>
  //   </List>
  // </Container>
}
const mapStateToProps = (state) => ({
  auth: state.auth.isAuth,
  data: state.users.user.data ?? {},
});
const mapDispatchToProps = (dispatch) => ({
  getUser: (id) => dispatch(getUser(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
