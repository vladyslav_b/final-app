{
  /* Start */
}
{
  posts ? (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Title</TableCell>
            <TableCell>Content</TableCell>
            <TableCell>Author</TableCell>
            <TableCell>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {posts
            .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            .map((row, index) => (
              <TableRow
                key={row._id}
                sx={{
                  "&:last-child td, &:last-child th": {
                    border: 0,
                  },
                }}
              >
                <TableCell>
                  {row.title.length > 10
                    ? `${row.title.slice(0, 10)}...`
                    : `${row.title}`}
                </TableCell>
                <TableCell>
                  {row.content.length > 50
                    ? `${row.content.slice(0, 50)}...`
                    : `${row.content}`}
                </TableCell>
                <TableCell>{row.author.name}</TableCell>
                <TableCell>
                  <IconButton
                    onClick={() => {
                      choosePost(row._id);
                      openEditModal();
                    }}
                  >
                    <Edit />
                  </IconButton>

                  <IconButton
                    onClick={() => {
                      choosePost(row._id);
                      openDeleteModal();
                    }}
                  >
                    <Delete />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
      {/* TABLE PAGINATION */}
      <TablePagination
        component="div"
        count={posts.length}
        page={page}
        onPageChange={handleChangePage}
        rowsPerPage={rowsPerPage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </TableContainer>
  ) : null;
}

/*
fetch('https://finalprojectapi.magisoft.solutions/posts/', {
credentials: "include",
method: 'POST',
 headers: {
      'Content-Type': 'application/json'
    },
body: JSON.stringify(
{
    "title": "testssssssssssssssssssssssss",
    "content": "test 2"
}
)
}).then(e => console.log('+',e), e => console.log('-',e))
*/
