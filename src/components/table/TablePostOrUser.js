import { React, useState, useEffect } from "react";
import {
  IconButton,
  Table,
  Paper,
  TableCell,
  TableContainer,
  TableRow,
  TableHead,
  TableBody,
  TablePagination,
  Button,
  Box,
  Avatar,
  Pagination,
} from "@material-ui/core";
import { Edit, Delete } from "@material-ui/icons";
import { Link, useParams } from "react-router-dom";
import { connect } from "react-redux";
import Loader from "../loader/Loader";
import {
  choosePost,
  fullInfoChoosePost,
  editModalOpen,
  deleteModalOpen,
  deleteUserModalOpen,
  chooseUser,
  loadPostsByUser,
  loadUsers,
  loadPosts,
} from "../../actions/actionCreator";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(() => ({
  link: {
    textDecoration: "none",
    color: "#464646",
  },
}));

function TablePostOrUser({
  id,
  target,
  postsOrUser,
  choosePost,
  fullInfoChoosePost,
  editModalOpen,
  deleteModalOpen,
  deleteUserModalOpen,
  chooseUser,
  loadPostsByUser,
  loadUsers,
  loadPosts,
  isLoadingPosts,
  isLoadingUsers,
}) {
  const classes = useStyles();
  /* PAGINATION  */
  const [page, setPage] = useState(1);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  // const handleChangeRowsPerPage = (event) => {
  //   setRowsPerPage(10);
  //   setPage(0);
  // };
  let { param } = useParams();
  useEffect(() => {
    if (target === "posts") {
      loadPosts();
    } else if (target === "posts" && param == "myposts") {
      loadPostsByUser(id);
    } else if (target === "users") {
      loadPosts();
    }
  }, []);
  if (isLoadingPosts || isLoadingUsers) {
    return <Loader />;
  }
  if (target === "posts") {
    return (
      <>
        {postsOrUser ? (
          <TableContainer component={Paper} sx={{ boxShadow: "none" }}>
            <Table
              sx={{ minWidth: 650 }}
              size="small"
              aria-label="a dense table"
            >
              {/* <TableHead>
                <TableRow>
                  <TableCell>Title</TableCell>
                  <TableCell>Content</TableCell>
                  <TableCell>Author</TableCell>
                  <TableCell>Action</TableCell>
                </TableRow>
              </TableHead> */}
              <TableBody>
                {postsOrUser
                  .slice(
                    (page - 1) * rowsPerPage,
                    (page - 1) * rowsPerPage + rowsPerPage
                  )
                  .map((row, index) => (
                    <TableRow
                      key={row._id}
                      sx={{
                        td: {
                          color: "#464646",
                          fontSize: "16px",
                          padding: "20px",
                        },
                      }}
                    >
                      <TableCell
                        sx={{ width: "290px", fontSize: "20px !important" }}
                      >
                        <Link to={`/posts/${row._id}`} className={classes.link}>
                          {row.title.length > 10
                            ? `${row.title.slice(0, 10)}...`
                            : `${row.title}`}
                        </Link>
                      </TableCell>
                      <TableCell
                        sx={{ width: "390px", color: "#6c6c6c !important" }}
                      >
                        {row.content.length > 50
                          ? `${row.content.slice(0, 50)}...`
                          : `${row.content}`}
                      </TableCell>
                      <TableCell>
                        <Box sx={{ display: "flex" }}>
                          <Avatar
                            sx={{ transform: "scale(0.8)" }}
                            src={
                              row.author?.profileImg
                                ? row.author?.profileImg
                                : "./public/user.png"
                            }
                          />
                          <Box
                            sx={{
                              alignSelf: "center",
                              color: "#C4C4C4 !important",
                            }}
                          >
                            {row.author.name}
                          </Box>
                        </Box>
                      </TableCell>
                      <TableCell>
                        {/* <Link to={`/posts/${row._id}`}>
                          <Button
                            variant="outlined"
                            onClick={() => {
                              fullInfoChoosePost(row);
                            }}
                          >
                            Details
                          </Button>
                        </Link> */}

                        <IconButton
                          onClick={() => {
                            choosePost(row._id);
                            fullInfoChoosePost(row);
                            editModalOpen();
                          }}
                        >
                          <Edit />
                        </IconButton>
                        <IconButton
                          onClick={() => {
                            choosePost(row._id);
                            deleteModalOpen();
                          }}
                        >
                          <Delete />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>

            {/* TABLE PAGINATION */}
            {/* <TablePagination
              component="div"
              count={postsOrUser.length}
              page={page}
              onPageChange={handleChangePage}
              rowsPerPage={rowsPerPage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            /> */}
            <Pagination
              sx={{
                margin: "60px auto",
                textAlign: "center",
                display: "flex",
                justifyContent: "center",
              }}
              count={Math.ceil(postsOrUser.length / 10)}
              page={page}
              onChange={handleChangePage}
            />
          </TableContainer>
        ) : null}
      </>
    );
  }
  if (target === "users") {
    return (
      <>
        {postsOrUser ? (
          <TableContainer component={Paper} sx={{ boxShadow: "none" }}>
            <Table
              sx={{ minWidth: 650 }}
              size="small"
              aria-label="a dense table"
            >
              {/* <TableHead>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>Email</TableCell>
                  <TableCell>City</TableCell>
                  <TableCell>Street</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead> */}
              <TableBody>
                {postsOrUser
                  .slice(
                    (page - 1) * rowsPerPage,
                    (page - 1) * rowsPerPage + rowsPerPage
                  )
                  // .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                  .map((row, index) => (
                    <TableRow
                      key={row._id}
                      sx={{
                        td: {
                          color: "#464646",
                          fontSize: "16px",
                          padding: "20px",
                        },
                      }}
                    >
                      <TableCell
                        sx={{ width: "290px", fontSize: "20px !important" }}
                      >
                        <Link to={`/users/${row._id}`} className={classes.link}>
                          {row.name}
                        </Link>
                      </TableCell>
                      <TableCell
                        sx={{ width: "390px", color: "#6c6c6c !important" }}
                      >
                        {row.email}
                      </TableCell>
                      <TableCell
                        sx={{ width: "390px", color: "#6c6c6c !important" }}
                      >
                        {row.address?.city}
                      </TableCell>
                      <TableCell
                        sx={{ width: "390px", color: "#6c6c6c !important" }}
                      >
                        {row.address?.street}
                      </TableCell>
                      <TableCell>
                        {/* <Link to={`/users/${row._id}`}>
                          <Button
                            variant="outlined"
                            onClick={() => {
                              chooseUser(row);
                            }}
                          >
                            Details
                          </Button>
                        </Link> */}

                        <IconButton
                          onClick={() => {
                            chooseUser(row);
                            deleteUserModalOpen();
                          }}
                        >
                          <Delete />
                        </IconButton>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>

            {/* TABLE PAGINATION */}
            {/* <TablePagination
              component="div"
              count={postsOrUser.length}
              page={page}
              onPageChange={handleChangePage}
              rowsPerPage={rowsPerPage}
              onRowsPerPageChange={handleChangeRowsPerPage}
            /> */}
            <Pagination
              sx={{
                margin: "60px auto",
                textAlign: "center",
                display: "flex",
                justifyContent: "center",
              }}
              count={Math.ceil(postsOrUser.length / 10)}
              page={page}
              onChange={handleChangePage}
            />
          </TableContainer>
        ) : null}
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  choosenPost: state.posts.choosenPost,
  choosenUser: state.users.choosenUser,
  id: state.users.user.data._id,
  isLoadingPosts: state.posts.isLoading,
  isLoadingUsers: state.users.isLoading,
});

const mapDispatchToProps = (dispatch) => ({
  loadPostsByUser: (id) => dispatch(loadPostsByUser(id)),
  loadUsers: () => dispatch(loadUsers()),
  loadPosts: () => dispatch(loadPosts()),
  choosePost: (id) => dispatch(choosePost(id)),
  fullInfoChoosePost: (row) => dispatch(fullInfoChoosePost(row)),
  editModalOpen: () => dispatch(editModalOpen()),
  deleteModalOpen: () => dispatch(deleteModalOpen()),
  deleteUserModalOpen: () => dispatch(deleteUserModalOpen()),
  chooseUser: (id) => dispatch(chooseUser(id)),
});
export default connect(mapStateToProps, mapDispatchToProps)(TablePostOrUser);
