import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  AppBar,
  Button,
  Typography,
  Container,
  Toolbar,
  Box,
  Avatar,
  IconButton,
  Menu,
  MenuItem,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { logOut } from "../../actions/actionCreator";
import SettingsIcon from "@material-ui/icons/Settings";

const useStyles = makeStyles(() => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,

    textDecoration: "none",
  },
  title_a: {
    color: "#6C6C6C",
    fontSize: "20px",
    fontWeight: "400",
    textDecoration: "none",
    "&:hover": {
      color: "#000",
    },
  },
  login: {
    color: "#6C6C6C",
    textDecoration: "none",
    "&:hover": {
      color: "#000",
    },
  },
  sign: {
    color: "#6C6C6C",
    textDecoration: "none",
    "&:hover": {
      color: "#000",
    },
  },
  avatar: {
    transform: "scale(0.9)",
    marginRight: "10px",
  },
  icon: {
    transform: "scale(1.7)",
  },
}));

function Header(props) {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const classes = useStyles();
  return (
    <AppBar
      position="static"
      color="lite"
      sx={{ minHeight: "80px", alignItems: "center", boxShadow: "none" }}
    >
      <Container>
        <Toolbar sx={{ minHeight: "80px !important" }}>
          <Typography variant="h6" className={classes.title} color="primary">
            <Link className={classes.title_a} to="/">
              FINAL PROJECT
            </Link>
          </Typography>
          <Box sx={{ mr: 2 }}>
            {localStorage.getItem("id") ? (
              <Box sx={{ display: "flex" }}>
                <Typography sx={{ padding: "10px" }}>
                  {props.user?.name}
                </Typography>
                <Avatar
                  className={classes.avatar}
                  src={
                    props.user?.profileImg
                      ? props.user?.profileImg
                      : "./public/user.png"
                  }
                />
                <IconButton
                  id="basic-button"
                  aria-controls="basic-menu"
                  aria-haspopup="true"
                  aria-expanded={open ? "true" : undefined}
                  onClick={handleClick}
                >
                  <SettingsIcon className={classes.icon} />
                </IconButton>
                <Menu
                  id="basic-menu"
                  anchorEl={anchorEl}
                  open={open}
                  onClose={handleClose}
                  MenuListProps={{
                    "aria-labelledby": "basic-button",
                  }}
                >
                  <MenuItem onClick={handleClose}>
                    <Link className={classes.sign} to="/myprofile">
                      Profile
                    </Link>
                  </MenuItem>
                  <MenuItem
                    onClick={() => {
                      props.logOut();
                      handleClose();
                    }}
                  >
                    <Link className={classes.sign} to="/">
                      Log out
                    </Link>
                  </MenuItem>
                </Menu>
                {/* <Button
                  color="primary"
                  variant="outlined"
                  className={classes.sign}
                  onClick={() => {
                    
                  }}
                >
                  
                </Button> */}
              </Box>
            ) : (
              <Box>
                <Button
                  color="primary"
                  variant="text"
                  className={classes.login}
                >
                  <Link className={classes.login} to="/login">
                    LOG IN
                  </Link>
                </Button>
                <span>/</span>
                <Button color="primary" variant="text" className={classes.sign}>
                  <Link className={classes.sign} to="/register">
                    REGISTER
                  </Link>
                </Button>
              </Box>
            )}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
const mapStateToProps = (state) => ({
  auth: state.auth,
  user: state.users.user.data,
});
const mapDispatchToProps = (dispatch) => ({
  logOut: () => dispatch(logOut()),
});
export default connect(mapStateToProps, mapDispatchToProps)(Header);
