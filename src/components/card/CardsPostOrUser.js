import { React, useState, useEffect } from "react";
import {
  IconButton,
  Grid,
  Card,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  Pagination,
  Stack,
  Button,
  Box,
  Avatar,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import { Edit, Delete } from "@material-ui/icons";
import { connect } from "react-redux";
import {
  choosePost,
  fullInfoChoosePost,
  editModalOpen,
  deleteModalOpen,
  loadPostsPagination,
  loadPostsByUserPagination,
  loadUsersPagination,
  deleteUserModalOpen,
  chooseUser,
} from "../../actions/actionCreator";

function CardsPostOrUser({
  target,
  disp,
  idUser,
  count,
  countByUser,
  postsOrUser,
  choosePost,
  fullInfoChoosePost,
  editModalOpen,
  deleteModalOpen,
  loadPostsPagination,
  loadPostsByUserPagination,
  countUsers,
  loadUsersPagination,
  deleteUserModalOpen,
  chooseUser,
}) {
  const [page, setPage] = useState(
    sessionStorage.getItem("page") ? Number(sessionStorage.getItem("page")) : 1
  );

  //bag with 2 requests
  useEffect(() => {
    if (idUser && disp == "2") {
      loadPostsByUserPagination({ page: page, id: idUser });
    }
    if (target == "posts" && disp == "1") {
      loadPostsPagination(page);
    }
  }, [page]);

  const [pageUsers, setPageUsers] = useState(
    sessionStorage.getItem("pageUsers")
      ? Number(sessionStorage.getItem("pageUsers"))
      : 1
  );
  //bag with 2 requests

  useEffect(() => {
    if (target == "users") {
      loadUsersPagination(pageUsers);
    }
  }, [pageUsers]);
  if (target === "posts") {
    return (
      <>
        {postsOrUser ? (
          <Grid
            container
            sx={{
              width: "600px",
              padding: "5px",
              minHeight: "50px",
              backgroundColor: "#fff",
            }}
            spacing={{ xs: 2, md: 3 }}
          >
            {postsOrUser.map((item, index) => (
              <Grid item xs={2} sm={4} md={4} key={item._id}>
                <Card sx={{ maxWidth: 320 }}>
                  <CardMedia
                    component="img"
                    alt="this will be Post Promo"
                    height="200"
                    image="https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg"
                  />
                  <CardContent>
                    <Typography
                      gutterBottom
                      variant="cardTitle"
                      component="div"
                    >
                      {item.title}
                    </Typography>
                    <Typography typography="cardContext" color="text.secondary">
                      {item.content}
                    </Typography>
                  </CardContent>
                  <Box
                    sx={{
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "16px",
                    }}
                  >
                    <Box sx={{ display: "flex" }}>
                      <Avatar
                        src={
                          item.author?.profileImg
                            ? item.author?.profileImg
                            : "./public/user.png"
                        }
                      />
                      <Link
                        to={`/posts/${item._id}`}
                        style={{ color: "#C4C4C4", textDecoration: "none" }}
                      >
                        <Typography sx={{ padding: "10px" }}>
                          {item.author.name}
                        </Typography>
                      </Link>
                    </Box>
                    {/* <Link to={`/posts/${item._id}`}>
                      <Button
                        variant="outlined"
                        onClick={() => {
                          fullInfoChoosePost(item);
                        }}
                      >
                        Details
                      </Button>
                    </Link> */}
                    <Box>
                      <IconButton
                        onClick={() => {
                          choosePost(item._id);
                          fullInfoChoosePost(item);
                          editModalOpen();
                        }}
                      >
                        <Edit />
                      </IconButton>

                      <IconButton
                        onClick={() => {
                          choosePost(item._id);
                          deleteModalOpen();
                        }}
                      >
                        <Delete />
                      </IconButton>
                    </Box>
                  </Box>
                </Card>
              </Grid>
            ))}
            <Stack spacing={2} sx={{ width: "100%" }}>
              <Pagination
                count={
                  disp == "1"
                    ? Math.ceil(count / 6)
                    : Math.ceil(countByUser / 6)
                }
                sx={{
                  margin: "60px auto",
                  textAlign: "center",
                  display: "flex",
                  justifyContent: "center",
                }}
                color="secondary"
                variant="outlined"
                page={page}
                onChange={(event, value) => {
                  sessionStorage.setItem("page", value);
                  setPage(value);
                  {
                    disp == "2"
                      ? loadPostsByUserPagination({ page: page, id: idUser })
                      : loadPostsPagination(page);
                  }
                }}
              />
            </Stack>
          </Grid>
        ) : null}
      </>
    );
  }
  if (target == "users") {
    return (
      <>
        {postsOrUser ? (
          <Grid
            container
            sx={{
              width: "600px",
              padding: "5px",
              minHeight: "50px",
              backgroundColor: "#fff",
            }}
            spacing={{ xs: 2, md: 3 }}
          >
            {postsOrUser.map((item, index) => (
              <Grid item xs={2} sm={4} md={4} key={item._id}>
                <Card sx={{ maxWidth: 320, minHeight: 350 }}>
                  <CardMedia
                    component="img"
                    alt="green iguana"
                    height="200"
                    image={
                      item?.profileImg
                        ? item?.profileImg
                        : "https://miro.medium.com/max/1200/1*mk1-6aYaf_Bes1E3Imhc0A.jpeg"
                    }
                  />
                  <CardContent>
                    <Box
                      sx={{ display: "flex", justifyContent: "space-between" }}
                    >
                      <Typography typography="cardTitle" component="div">
                        {item.name}
                      </Typography>
                      <IconButton
                        onClick={() => {
                          chooseUser(item);
                          deleteUserModalOpen();
                        }}
                      >
                        <Delete />
                      </IconButton>
                    </Box>
                    <Typography typography="cardContent">
                      {item.email}
                    </Typography>
                    <Typography typography="cardContent">
                      {item?.address?.city}
                    </Typography>
                    <Typography typography="cardContent">
                      {item?.address?.street}
                    </Typography>
                  </CardContent>
                  {/* <CardActions>
                    <Link to={`/users/${item._id}`}>
                      <Button
                        variant="outlined"
                        onClick={() => {
                          chooseUser(item);
                        }}
                      >
                        Details
                      </Button>
                    </Link>
                    <IconButton
                      onClick={() => {
                        chooseUser(item);
                        deleteUserModalOpen();
                      }}
                    >
                      <Delete />
                    </IconButton>
                  </CardActions> */}
                </Card>
              </Grid>
            ))}
            <Stack spacing={2} sx={{ width: "100%" }}>
              <Pagination
                count={
                  disp == "3"
                    ? Math.ceil(countUsers / 6)
                    : Math.ceil(countUsers / 6)
                }
                sx={{
                  margin: "60px auto",
                  textAlign: "center",
                  display: "flex",
                  justifyContent: "center",
                }}
                color="secondary"
                variant="outlined"
                page={pageUsers}
                onChange={(event, value) => {
                  setPageUsers(value);
                  sessionStorage.setItem("pageUsers", value);
                  loadUsersPagination(pageUsers);
                  /* {
                    disp == "4"
                      ? loadUsersPagination(page)
                      : loadUsersPagination(page);
                  } */
                }}
              />
            </Stack>
          </Grid>
        ) : null}
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  postsPagination: state.posts.loadPostsPagination,
  count: state.posts.count,
  countByUser: state.posts.countByUser,
  choosenPost: state.posts.choosenPost,
  countUsers: state.users.countPagination,
  choosenUser: state.users.choosenUser,
});

const mapDispatchToProps = (dispatch) => ({
  choosePost: (id) => dispatch(choosePost(id)),
  fullInfoChoosePost: (row) => dispatch(fullInfoChoosePost(row)),
  editModalOpen: () => dispatch(editModalOpen()),
  deleteModalOpen: () => dispatch(deleteModalOpen()),
  loadPostsPagination: (page) => dispatch(loadPostsPagination(page)),
  loadPostsByUserPagination: (page) =>
    dispatch(loadPostsByUserPagination(page)),
  loadUsersPagination: (page) => dispatch(loadUsersPagination(page)),
  deleteUserModalOpen: () => dispatch(deleteUserModalOpen()),
  chooseUser: (id) => dispatch(chooseUser(id)),
});
export default connect(mapStateToProps, mapDispatchToProps)(CardsPostOrUser);
