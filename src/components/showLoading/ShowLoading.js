import React from "react";
import { Button, Container, Grid, Box } from "@material-ui/core";
import Loader from "../loader/Loader";
import Sidebar from "../sidebar/Sidebar";

export const LoaderWrap = () => {
  return (
    <div>
      <div>
        <Container>
          <Grid container>
            <Grid item md={2}>
              <Sidebar />
            </Grid>
            <Grid item md={10}>
              <Loader />
            </Grid>
          </Grid>
        </Container>
      </div>
    </div>
  );
};

export const ShowLoading = (isLoading, code) => {
  if (isLoading) {
    return <LoaderWrap />;
  } else {
    return code;
  }
};
