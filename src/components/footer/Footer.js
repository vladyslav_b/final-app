import React from "react";
import { connect } from "react-redux";

import { makeStyles } from "@material-ui/styles";
import { AppBar, Container, Typography } from "@material-ui/core";

const useStyles = makeStyles({
  footer: {
    display: "block",
    textAlign: "center",
    minHeight: "80px",
    padding: "35px",
    fontSize: "16px",
    lineHeight: "19px",
    color: "#464646",
  },
});
function Footer() {
  const classes = useStyles();
  return (
    <AppBar color="lite" position="static" sx={{ boxShadow: "none" }}>
      <Container bgcolot="primary.main">
        <Typography className={classes.footer}>This is Footer</Typography>
      </Container>
    </AppBar>
  );
}
export default connect(null, null)(Footer);
