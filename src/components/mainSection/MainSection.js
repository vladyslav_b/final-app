import { Container } from "@material-ui/core";
import React from "react";
import { connect } from "react-redux";

function MainSection(props) {
  return <Container sx={{ minHeight: "70vh" }}>{props.children}</Container>;
}
export default connect(null, null)(MainSection);
