import { React, useState, useEffect } from "react";
import { useFormik } from "formik";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  Box,
} from "@material-ui/core";
import {
  editPost,
  choosePost,
  editModalClose,
} from "../../actions/actionCreator";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(() => ({
  dialog: {
    "& div": {
      "& .MuiPaper-root": {
        width: "768px !important",
      },
    },
  },
  label: {
    marginTop: "20px",
    color: "#6C6C6C",
  },
  btnWrraper: {
    marginTop: "40px",
    display: "flex",
    justifyContent: "center",
    "& button": {
      width: "170px",
      height: "40px",
      borderRadius: "0",
    },
  },
}));

function EditModal({ infoPost, choosenPost, editPost, edit, editModalClose }) {
  const classes = useStyles();
  // EDIT HANDLERS
  const [openEdit, setOpenEdit] = useState(false);

  const openEditModal = () => {
    setOpenEdit(true);
  };

  const closeEditModal = () => {
    setOpenEdit(false);
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      title: infoPost.title ?? "wtf",
      content: infoPost.content,
    },
    onSubmit: (values) => {
      editPost(choosenPost, values);
      editModalClose();
    },
  });

  useEffect(() => {
    edit ? openEditModal() : closeEditModal();
  });
  return (
    <>
      {/* EDIT MODAL */}
      <Dialog
        open={openEdit}
        onClose={closeEditModal}
        className={classes.dialog}
        maxWidth={"lg"}
        fullWidth={true}
      >
        <DialogTitle>Edit Post {choosenPost}</DialogTitle>
        <DialogContent>
          <form onSubmit={formik.handleSubmit}>
            <label className={classes.label}>Title</label>
            <TextField
              fullWidth
              id="title"
              name="title"
              sx={{
                marginTop: "5px",

                div: {
                  borderRadius: "0",
                  input: {
                    padding: "5px",
                    wordWrap: "wrap",
                  },
                },
              }}
              value={formik.values.title}
              onChange={formik.handleChange}
              error={formik.touched.title && Boolean(formik.errors.title)}
              helperText={formik.touched.title && formik.errors.title}
            />
            <label className={classes.label}>Content</label>
            <TextField
              fullWidth
              id="content"
              name="content"
              multiline
              rows={4}
              sx={{
                marginTop: "5px",
                div: {
                  borderRadius: "0",
                  input: {
                    height: "135px",
                  },
                },
              }}
              value={formik.values.content}
              onChange={formik.handleChange}
              error={formik.touched.content && Boolean(formik.errors.content)}
              helperText={formik.touched.content && formik.errors.content}
            />

            <Box className={classes.btnWrraper}>
              <Button
                color="primary"
                variant="contained"
                onClick={editModalClose}
                fullWidth
                sx={{ marginRight: "30px" }}
              >
                Cancel
              </Button>
              <Button
                color="primary"
                variant="contained"
                fullWidth
                type="submit"
              >
                EDIT THIS POST
              </Button>
            </Box>
          </form>
        </DialogContent>
      </Dialog>
    </>
  );
}
const mapStateToProps = (state) => ({
  infoPost: state.posts.fullInfoChoosePost,
  edit: state.modal.editModal,
  choosenPost: state.posts.choosenPost,
});

const mapDispatchToProps = (dispatch) => ({
  choosePost: (id) => dispatch(choosePost(id)),
  editPost: (choosenPost, payload) => dispatch(editPost(choosenPost, payload)),
  editModalClose: () => dispatch(editModalClose()),
});
export default connect(mapStateToProps, mapDispatchToProps)(EditModal);
