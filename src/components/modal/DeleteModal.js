import { React, useState, useEffect } from "react";

import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Typography,
} from "@material-ui/core";
import {
  deletePost,
  choosePost,
  deleteModalClose,
} from "../../actions/actionCreator";
import { connect } from "react-redux";
import { Box } from "@material-ui/system";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(() => ({
  dialog: {
    "& div": {
      "& .MuiPaper-root": {
        width: "470px !important",
      },
    },
  },
  btnWrraper: {
    marginTop: "40px",
    display: "flex",
    paddingBottom: "40px",
    justifyContent: "center",
    // "& button": {
    //   width: "170px",
    //   height: "40px",
    //   borderRadius: "0",
    // },
  },
}));

function DeleteModal({ choosenPost, deletePost, deleteP, deleteModalClose }) {
  const classes = useStyles();
  // DETELE HANDLERS
  const [openDelete, setOpenDelete] = useState(false);
  const openDeleteModal = () => {
    setOpenDelete(true);
  };
  const closeDeleteModal = () => {
    setOpenDelete(false);
  };

  useEffect(() => {
    deleteP ? openDeleteModal() : closeDeleteModal();
  });
  return (
    <>
      {/* DELETE MODAL */}
      <Dialog
        // ;
        className={classes.dialog}
        fullWidth={true}
        maxWidth={"lg"}
        open={openDelete}
        onClose={closeDeleteModal}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="alert-dialog-title"
          align="center"
          sx={{ margin: "40px 25px 0" }}
        >
          ARE YOU SURE THAT U WONNA DELETE THIS POST
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description" align="center">
            POST ID IS - {choosenPost}
          </DialogContentText>
        </DialogContent>
        <Box className={classes.btnWrraper}>
          <Button
            color="primary"
            variant="chooseBtn"
            onClick={() => {
              deletePost(choosenPost);
              deleteModalClose();
            }}
            sx={{ marginRight: "30px" }}
          >
            <Typography>Yes</Typography>
          </Button>
          <Button
            color="primary"
            variant="chooseBtn"
            onClick={deleteModalClose}
          >
            <Typography>No</Typography>
          </Button>
        </Box>
      </Dialog>
    </>
  );
}
const mapStateToProps = (state) => ({
  deleteP: state.modal.deleteModal,
  choosenPost: state.posts.choosenPost,
});

const mapDispatchToProps = (dispatch) => ({
  deletePost: (id) => dispatch(deletePost(id)),
  choosePost: (id) => dispatch(choosePost(id)),
  deleteModalClose: () => dispatch(deleteModalClose()),
});
export default connect(mapStateToProps, mapDispatchToProps)(DeleteModal);
