import { React, useState, useEffect } from "react";

import { useFormik } from "formik";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
  Box,
} from "@material-ui/core";
import { createPost, createModalClose } from "../../actions/actionCreator";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(() => ({
  dialog: {
    "& div": {
      "& .MuiPaper-root": {
        width: "768px !important",
      },
    },
  },
  label: {
    marginTop: "20px",
    color: "#6C6C6C",
  },
  btnWrraper: {
    marginTop: "40px",
    display: "flex",
    justifyContent: "center",
    "& button": {
      width: "170px",
      height: "40px",
      borderRadius: "0",
    },
  },
}));

function CreateModal({ createPost, create, createModalClose }) {
  //CREATE POST
  const classes = useStyles();
  const [openCreate, setOpenCreate] = useState(false);
  const openCreateModal = () => {
    setOpenCreate(true);
  };

  const closeCreateModal = () => {
    setOpenCreate(false);
  };

  const formik2 = useFormik({
    enableReinitialize: true,
    initialValues: {
      title: "",
      content: "",
    },
    onSubmit: (values, { resetForm }) => {
      createPost(values);
      createModalClose();
      resetForm();
    },
  });
  useEffect(() => {
    create ? openCreateModal() : closeCreateModal();
  });

  return (
    <>
      {/* CREATE MODAL */}
      <Dialog
        open={openCreate}
        onClose={closeCreateModal}
        className={classes.dialog}
        maxWidth={"lg"}
        fullWidth={true}
      >
        <DialogTitle sx={{ textAlign: "center" }}>New Post</DialogTitle>
        <DialogContent>
          <form onSubmit={formik2.handleSubmit}>
            <label className={classes.label}>Title</label>
            <TextField
              fullWidth
              id="titleCreate"
              name="title"
              type="text"
              size="small"
              sx={{
                marginTop: "5px",

                div: {
                  borderRadius: "0",
                  input: {
                    padding: "5px",
                  },
                },
              }}
              value={formik2.values.title}
              onChange={formik2.handleChange}
              error={formik2.touched.title && Boolean(formik2.errors.title)}
              helperText={formik2.touched.title && formik2.errors.title}
            />
            <label className={classes.label}>Content</label>
            <TextField
              fullWidth
              id="contentCreate"
              name="content"
              multiline
              rows={4}
              sx={{
                marginTop: "5px",
                div: {
                  borderRadius: "0",
                  input: {
                    padding: "0",
                    minHeight: "135px",
                    wordWrap: "wrap",
                  },
                },
              }}
              value={formik2.values.content}
              onChange={formik2.handleChange}
              error={formik2.touched.content && Boolean(formik2.errors.content)}
              helperText={formik2.touched.content && formik2.errors.content}
            />
            <Box className={classes.btnWrraper}>
              <Button
                color="primary"
                variant="contained"
                onClick={() => {
                  createModalClose();
                }}
                sx={{ marginRight: "30px" }}
              >
                Cancel
              </Button>
              <Button color="primary" variant="contained" type="submit">
                CREATE NEW POST
              </Button>
            </Box>
          </form>
        </DialogContent>
        <DialogActions></DialogActions>
      </Dialog>
    </>
  );
}
const mapStateToProps = (state) => ({
  create: state.modal.createModal,
});

const mapDispatchToProps = (dispatch) => ({
  createPost: (values) => dispatch(createPost(values)),
  createModalClose: () => dispatch(createModalClose()),
});
export default connect(mapStateToProps, mapDispatchToProps)(CreateModal);
