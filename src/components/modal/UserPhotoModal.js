import { React, useState, useEffect } from "react";
import { useFormik } from "formik";
import { Formik } from "formik";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
} from "@material-ui/core";
import { connect } from "react-redux";
import {
  editPhoto,
  editPhotoModalClose,
  getUser,
} from "../../actions/actionCreator";

function UserPhotoModal(props) {
  //CREATE POST
  const [openPhoto, setOpenPhoto] = useState(false);
  const openPhotoModal = () => {
    setOpenPhoto(true);
  };

  const closePhotoModal = () => {
    setOpenPhoto(false);
  };

  const formik2 = useFormik({
    enableReinitialize: true,
    initialValues: {
      file: null,
    },
    onSubmit: (values, { resetForm }) => {
      const payload = {
        id: props.user.data._id,
        photo: values.file,
      };
      console.log("payload from modal", payload);
      props.editPhoto(payload);
      props.editPhotoModalClose();
      resetForm();
      setTimeout(() => {
        props.getUser(props.user.data._id);
      }, 2000);
    },
  });
  useEffect(() => {
    props.userPhoto ? openPhotoModal() : closePhotoModal();
  });

  return (
    <>
      {/* Photo MODAL */}
      <Dialog open={openPhoto} onClose={closePhotoModal}>
        <DialogTitle>Edit Photo</DialogTitle>
        <DialogContent>
          <form onSubmit={formik2.handleSubmit}>
            <div className="form-group">
              <label htmlFor="file">File upload</label>
              <input
                id="file"
                name="file"
                type="file"
                className="form-control"
                onChange={(event) => {
                  formik2.setFieldValue("file", event.currentTarget.files[0]);
                }}
              />
            </div>
            <Button
              onClick={() => {
                props.editPhotoModalClose();
              }}
              fullWidth
            >
              Cancel
            </Button>
            <Button color="primary" variant="contained" fullWidth type="submit">
              Edit Photo
            </Button>
          </form>
        </DialogContent>
      </Dialog>
    </>
  );
}
const mapStateToProps = (state) => ({
  userPhoto: state.modal.userPhotoModal,
  user: state.users.user,
});

const mapDispatchToProps = (dispatch) => ({
  editPhoto: (payload) => dispatch(editPhoto(payload)),
  editPhotoModalClose: () => dispatch(editPhotoModalClose()),
  getUser: (values) => dispatch(getUser(values)),
});
export default connect(mapStateToProps, mapDispatchToProps)(UserPhotoModal);
