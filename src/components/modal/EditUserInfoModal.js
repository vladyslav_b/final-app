import { React, useState, useEffect } from "react";

import { useFormik, Field } from "formik";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  TextField,
} from "@material-ui/core";
import {
  editUserInfo,
  editUserInfoModalClose,
  getUser,
} from "../../actions/actionCreator";
import { connect } from "react-redux";

function EditUserInfoModal({
  data,
  getUser,
  editUserInfo,
  editUserInfoModal,
  editUserInfoModalClose,
}) {
  //CREATE POST
  const [openEditUserInfo, setOpenEditUserInfo] = useState(false);
  const openEditUserInfoModal = () => {
    setOpenEditUserInfo(true);
  };

  const closeEditUserInfoModal = () => {
    setOpenEditUserInfo(false);
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      name: data.name,
      city: data?.address?.city,
      street: data?.address?.street,
      picked: data?.settings?.mode,
    },
    onSubmit: (values, { resetForm }) => {
      const obj = {
        id: data._id,
        name: values.name,
        address: {
          city: values.city,
          street: values.street,
        },
        settings: {
          mode: values.picked,
        },
      };
      editUserInfo(obj);
      editUserInfoModalClose();

      resetForm();
      setTimeout(() => {
        getUser(data._id);
      }, 1000);
    },
  });
  useEffect(() => {
    editUserInfoModal ? openEditUserInfoModal() : closeEditUserInfoModal();
  });

  return (
    <>
      <Dialog open={openEditUserInfo} onClose={closeEditUserInfoModal}>
        <DialogTitle>CHANGE USER INFO</DialogTitle>
        <DialogContent>
          <form onSubmit={formik.handleSubmit}>
            <TextField
              fullWidth
              required
              id="name"
              name="name"
              label="Name"
              type="name"
              sx={{ marginBottom: "10px", marginTop: "10px" }}
              value={formik.values.name}
              onChange={formik.handleChange}
              error={formik.touched.name && Boolean(formik.errors.name)}
              helperText={formik.touched.name && formik.errors.name}
            />
            <TextField
              fullWidth
              id="city"
              name="city"
              label="City"
              type="text"
              sx={{ marginBottom: "10px" }}
              value={formik.values.city}
              onChange={formik.handleChange}
              error={formik.touched.city && Boolean(formik.errors.city)}
              helperText={formik.touched.city && formik.errors.city}
            />
            <TextField
              fullWidth
              id="street"
              name="street"
              label="Street"
              type="text"
              sx={{ marginBottom: "10px" }}
              value={formik.values.street}
              onChange={formik.handleChange}
              error={formik.touched.street && Boolean(formik.errors.street)}
              helperText={formik.touched.street && formik.errors.street}
            />
            <label>
              <input
                type="radio"
                name="picked"
                value="TABLE"
                checked={formik.values.picked === "TABLE"}
                onChange={() => formik.setFieldValue("picked", "TABLE")}
              />
              TABLE
            </label>
            <label>
              <input
                type="radio"
                name="picked"
                value="BOXES"
                checked={formik.values.picked === "BOXES"}
                onChange={() => formik.setFieldValue("picked", "BOXES")}
              />
              BOXES
            </label>
            <Button
              onClick={() => {
                editUserInfoModalClose();
                getUser(data._id);
              }}
              fullWidth
            >
              Cancel
            </Button>
            <Button color="primary" variant="contained" fullWidth type="submit">
              CHANGE USER INFO
            </Button>
          </form>
        </DialogContent>
        <DialogActions></DialogActions>
      </Dialog>
    </>
  );
}
const mapStateToProps = (state) => ({
  editUserInfoModal: state.modal.editUserInfoModal,
  data: state.users.user.data ?? {},
});

const mapDispatchToProps = (dispatch) => ({
  editUserInfo: (values) => dispatch(editUserInfo(values)),
  getUser: (values) => dispatch(getUser(values)),
  editUserInfoModalClose: () => dispatch(editUserInfoModalClose()),
});
export default connect(mapStateToProps, mapDispatchToProps)(EditUserInfoModal);
