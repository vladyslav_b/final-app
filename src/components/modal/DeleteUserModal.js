import { React, useState, useEffect } from "react";

import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Box,
} from "@material-ui/core";
import {
  deletePost,
  choosePost,
  deleteUserModalClose,
  deleteUser,
} from "../../actions/actionCreator";
import { connect } from "react-redux";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles(() => ({
  dialog: {
    "& div": {
      "& .MuiPaper-root": {
        width: "470px !important",
      },
    },
  },
  btnWrraper: {
    marginTop: "40px",
    display: "flex",
    paddingBottom: "40px",
    justifyContent: "center",
    "& button": {
      width: "170px",
      height: "40px",
      borderRadius: "0",
    },
  },
}));

function DeleteUserModal({
  choosenUser,
  choosenPost,
  deletePost,
  deleteU,
  deleteUserModalClose,
  deleteUser,
}) {
  const classes = useStyles();
  // DETELE HANDLERS
  const [openDelete, setOpenDelete] = useState(false);
  const openDeleteModal = () => {
    setOpenDelete(true);
  };
  const closeDeleteModal = () => {
    setOpenDelete(false);
  };

  useEffect(() => {
    deleteU ? openDeleteModal() : closeDeleteModal();
  });
  return (
    <>
      {/* DELETE MODAL */}
      <Dialog
        className={classes.dialog}
        maxWidth={"lg"}
        fullWidth={true}
        open={openDelete}
        onClose={closeDeleteModal}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle
          id="alert-dialog-title"
          align="center"
          sx={{ margin: "40px 25px 0" }}
        >
          ARE YOU SURE THAT U WONNA DELETE THIS USER
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description" align="center">
            USER Name IS -{choosenUser?.name}
          </DialogContentText>
        </DialogContent>
        <Box className={classes.btnWrraper}>
          <Button
            color="primary"
            variant="contained"
            onClick={() => {
              deleteUser(choosenUser._id);
              deleteUserModalClose();
            }}
            sx={{ marginRight: "30px" }}
          >
            Yes
          </Button>
          <Button
            color="primary"
            variant="contained"
            onClick={deleteUserModalClose}
          >
            No
          </Button>
        </Box>
      </Dialog>
    </>
  );
}
const mapStateToProps = (state) => ({
  deleteU: state.modal.deleteUserModal,
  choosenUser: state.users.choosenUser,
});

const mapDispatchToProps = (dispatch) => ({
  deletePost: (id) => dispatch(deletePost(id)),
  choosePost: (id) => dispatch(choosePost(id)),
  deleteUserModalClose: () => dispatch(deleteUserModalClose()),
  deleteUser: (id) => dispatch(deleteUser(id)),
});
export default connect(mapStateToProps, mapDispatchToProps)(DeleteUserModal);
