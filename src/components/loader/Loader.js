import React from "react";
import { connect } from "react-redux";
import "./loader.scss";

function Loader(props) {
  return <div className="loader"></div>;
}

export default connect(null, null)(Loader);
