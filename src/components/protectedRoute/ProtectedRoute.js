import React from "react";
import { Redirect, Route } from "react-router-dom";
import { connect } from "react-redux";

function ProtectedRoute(props) {
  const { redirectPath, component, auth, loggedIn, ...routeProps } = props;
  const Component = component;
  const isAccessible = Boolean(auth);
  console.log(isAccessible);
  return (
    <Route
      {...routeProps}
      render={(props) => {
        if (isAccessible) return <Component {...props} />;
        return (
          <Redirect to={{ pathname: redirectPath || "/notautorized401" }} />
        );
      }}
    />
  );
}
const mapStateToProps = (state) => ({
  auth: state.auth.isAuth,
});
export default connect(mapStateToProps, null)(ProtectedRoute);
