import { combineReducers } from "redux";
import { authReducer } from "./authReducer";
import { postsReducer } from "./postsReducer";
import { usersReducer } from "./usersReducer";
import { modalReducer } from "./modalReducer";
import { errorReducer } from "./errorReducer";

export const rootReducer = combineReducers({
  auth: authReducer,
  posts: postsReducer,
  users: usersReducer,
  modal: modalReducer,
  error: errorReducer,
});
