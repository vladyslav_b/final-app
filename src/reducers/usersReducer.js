import {
  GET_USER,
  GET_USER_RESPONSE,
  LOAD_USERS,
  LOAD_USERS_RESPONSE,
  DELETE_USER,
  DELETE_USER_RESPONSE,
  EDIT_PHOTO,
  EDIT_PHOTO_RESPONSE,
  EDIT_USER_INFO,
  EDIT_USER_INFO_RESPONSE,
  CHOOSE_USER,
  LOAD_USERS_PAGINATION,
  LOAD_USERS_PAGINATION_RESPONSE,
  SEARCH_USERS,
  GET_SPECIFIC_USER,
  GET_SPECIFIC_USER_RESPONSE,
} from "../actions/actionsType";

const initialState = {
  user: "",
  isLoading: true,
  specificUser: "",
};

export const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_USER:
      return { ...state, isLoading: true };
    case GET_USER_RESPONSE:
      return { ...state, user: action.payload, isLoading: false };
    case GET_SPECIFIC_USER:
      return { ...state, isLoading: true };
    case GET_SPECIFIC_USER_RESPONSE:
      return { ...state, specificUser: action.payload, isLoading: false };
    case LOAD_USERS:
      return { ...state, isLoading: true };
    case LOAD_USERS_RESPONSE:
      return {
        ...state,
        loadedUsers: [...action.payload.results],
        count: action.payload.count,
        isLoading: false,
      };
    case LOAD_USERS_PAGINATION:
      return { ...state, isLoading: true };
    case LOAD_USERS_PAGINATION_RESPONSE:
      return {
        ...state,
        loadedUsersPagination: [...action.payload.results],
        countPagination: action.payload.count,
        isLoading: false,
      };
    case EDIT_PHOTO:
      return { ...state };
    case EDIT_PHOTO_RESPONSE:
      return { ...state };
    case EDIT_USER_INFO:
      return { ...state };
    case EDIT_USER_INFO_RESPONSE:
      return { ...state };
    case CHOOSE_USER: {
      return { ...state, choosenUser: action.payload };
    }
    case DELETE_USER:
      return { ...state };
    case DELETE_USER_RESPONSE:
      return { ...state };
    case SEARCH_USERS: {
      return { ...state, searchUsers: [...action.payload] };
    }
    default:
      return state;
  }
};
