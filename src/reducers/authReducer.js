import { accordionActionsClasses } from "@material-ui/core";
import {
  LOG_IN,
  LOG_OUT,
  SIGN_IN,
  LOG_IN_RESPONSE,
  SIGN_IN_RESPONSE,
  LOG_OUT_RESPONSE,
  CHECK_AUTH,
  CHECK_AUTH_RESPONSE,
} from "../actions/actionsType";

const initialState = {
  isAuth: false,
  isLoading: true,
  isError: false,
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOG_IN:
      return { ...state, isLoading: true };
    case LOG_IN_RESPONSE:
      return {
        ...state,
        ...action.payload,
        isLoading: false,
        isAuth: action.isAuth,
      };
    case LOG_OUT:
      return { ...state, isLoading: true };
    case LOG_OUT_RESPONSE:
      return { ...state, isAuth: false, isLoading: false };
    case SIGN_IN:
      return state;
    case SIGN_IN_RESPONSE:
      return state;
    case CHECK_AUTH:
      return { ...state, isLoading: true };
    case CHECK_AUTH_RESPONSE:
      return { ...state, isAuth: action.payload, isLoading: false };
    default:
      return state;
  }
};
