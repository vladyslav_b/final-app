import {
  LOAD_POSTS,
  LOAD_POSTS_RESPONSE,
  LOAD_POSTS_PAGINATION,
  LOAD_POSTS_PAGINATION_RESPONSE,
  LOAD_POSTS_BY_USER,
  LOAD_POSTS_BY_USER_RESPONSE,
  LOAD_POSTS_BY_USER_PAGINATION,
  LOAD_POSTS_BY_USER_PAGINATION_RESPONSE,
  LOAD_POSTS_BY_SELECTED_USER,
  EDIT_POST,
  EDIT_POST_RESPONSE,
  DELETE_POST,
  DELETE_POST_RESPONSE,
  CHOOSE_POST,
  FULL_INFO_CHOOSE_POST,
  SEARCH_POSTS,
  POSTS_TO_SHOW,
  GET_POST_DETAIL,
  GET_POST_DETAIL_RESPONSE,
} from "../actions/actionsType";

const initialState = {
  isLoading: true,
  choosenPost: "",
  fullInfoChoosePost: "",
  postsByUser: "",
  searchPosts: [],
  post: {},
};

export const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_POSTS:
      return { ...state, isLoading: true };
    case LOAD_POSTS_RESPONSE:
      return { ...state, loadPosts: [...action.payload], isLoading: false };
    case LOAD_POSTS_PAGINATION:
      return { ...state, isLoading: true };
    case LOAD_POSTS_PAGINATION_RESPONSE:
      return {
        ...state,
        loadPostsPagination: action.payload.results,
        count: action.payload.count,
        isLoading: false,
      };
    case EDIT_POST:
      return state;
    case EDIT_POST_RESPONSE:
      return state;
    case DELETE_POST:
      return state;
    case DELETE_POST_RESPONSE:
      return state;
    case CHOOSE_POST:
      return { ...state, choosenPost: action.payload };
    case FULL_INFO_CHOOSE_POST:
      return { ...state, fullInfoChoosePost: action.payload };
    case LOAD_POSTS_BY_USER:
      return { ...state, isLoading: true };
    case LOAD_POSTS_BY_USER_RESPONSE:
      return { ...state, postsByUser: [...action.payload], isLoading: false };
    case LOAD_POSTS_BY_USER_PAGINATION:
      return { ...state };
    case LOAD_POSTS_BY_USER_PAGINATION_RESPONSE:
      return {
        ...state,
        loadPostsByUserPagination: action.payload.results,
        countByUser: action.payload.count,
      };
    case LOAD_POSTS_BY_SELECTED_USER: {
      return { ...state, searchPosts: [...action.payload] };
    }
    case SEARCH_POSTS: {
      return { ...state, searchPosts: [...action.payload] };
    }
    case POSTS_TO_SHOW: {
      return { ...state, postsToShow: [...action.payload] };
    }
    case GET_POST_DETAIL: {
      return { ...state, isLoading: true };
    }
    case GET_POST_DETAIL_RESPONSE: {
      return { ...state, isLoading: false, post: action.payload };
    }
    default:
      return state;
  }
};
