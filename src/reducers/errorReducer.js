import { ERROR_AUTH } from "../actions/actionsType";

const initialState = {
  isError: "",
};

export const errorReducer = (state = initialState, action) => {
  switch (action.type) {
    case ERROR_AUTH:
      return { ...state, isError: action.payload };
    case ERROR_AUTH: {
      return { ...state };
    }
    default:
      return state;
  }
};
