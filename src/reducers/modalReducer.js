import {
  CREATE_MODAL_OPEN,
  CREATE_MODAL_CLOSE,
  EDIT_MODAL_OPEN,
  EDIT_MODAL_CLOSE,
  DELETE_MODAL_OPEN,
  DELETE_MODAL_CLOSE,
  EDIT_PHOTO_MODAL_OPEN,
  EDIT_PHOTO_MODAL_CLOSE,
  EDIT_USER_INFO_MODAL_OPEN,
  EDIT_USER_INFO_MODAL_CLOSE,
  DELETE_USER_MODAL_OPEN,
  DELETE_USER_MODAL_CLOSE,
} from "../actions/actionsType";

const initialState = {
  createModal: false,
  editModal: false,
  deleteModal: false,
  userPhotoModal: false,
  editUserInfoModal: false,
  deleteUserModal: false,
};

export const modalReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_MODAL_OPEN:
      return { ...state, createModal: true };
    case CREATE_MODAL_CLOSE:
      return { ...state, createModal: false };
    case EDIT_MODAL_OPEN:
      return { ...state, editModal: true };
    case EDIT_MODAL_CLOSE:
      return { ...state, editModal: false };
    case DELETE_MODAL_OPEN:
      return { ...state, deleteModal: true };
    case DELETE_MODAL_CLOSE:
      return { ...state, deleteModal: false };
    case DELETE_USER_MODAL_OPEN:
      return { ...state, deleteUserModal: true };
    case DELETE_USER_MODAL_CLOSE:
      return { ...state, deleteUserModal: false };
    case EDIT_PHOTO_MODAL_OPEN:
      return { ...state, userPhotoModal: true };
    case EDIT_PHOTO_MODAL_CLOSE:
      return { ...state, userPhotoModal: false };
    case EDIT_USER_INFO_MODAL_OPEN:
      return { ...state, editUserInfoModal: true };
    case EDIT_USER_INFO_MODAL_CLOSE:
      return { ...state, editUserInfoModal: false };
    default:
      return state;
  }
};
